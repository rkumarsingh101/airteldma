package com.softage.airteldms.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.softage.airteldms.R;
import com.softage.airteldms.adaptors.SpinnerArrayAdapter;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.EmployeeDetails;
import com.softage.airteldms.dao.FtpDetails;
import com.softage.airteldms.dao.statusDAO.StatusDAO;
import com.softage.airteldms.dao.userDetailsDAO.DocumentTypeDetail;
import com.softage.airteldms.imageCapture.CameraXImageCapture;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.LocalPersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;

public class APBCapUploadVerification extends AppCompatActivity {
    private AppCompatEditText edit_text_mobile_no;
    private AppCompatEditText edit_text_mobile_no_reenter;
    private AppCompatSpinner spinner_select_doc_type;
    private MaterialButton btn_submit_detail;
    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private SpinnerArrayAdapter mSpinnerDocTypeArrayAdapter;
    private DocumentTypeDetail selectedDocTypeDetail = null;
    private String TAG = APBCapUploadVerification.class.getSimpleName();
    private ArrayList<DocumentTypeDetail> documentTypeDetailArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apb_cap_upload_verification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(getResources().getString(R.string.nav_item_cap_upload));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        edit_text_mobile_no = (AppCompatEditText) findViewById(R.id.edit_text_mobile_no);
        edit_text_mobile_no_reenter = (AppCompatEditText) findViewById(R.id.edit_text_mobile_no_reenter);
        edit_text_mobile_no.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        edit_text_mobile_no_reenter.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        spinner_select_doc_type = (AppCompatSpinner) findViewById(R.id.spinner_select_doc_type);
        btn_submit_detail = (MaterialButton) findViewById(R.id.btn_submit_detail);
        btn_submit_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CheckNetworkConnection.isNetworkAvailable(APBCapUploadVerification.this)) {

                    try {
                        if (isAllFieldValidated()) {
                            updateScannedOrderIDToServer();
                        }
                    } catch (Exception e) {
                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        e.printStackTrace();
                    }

                } else {
                    GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }

            }
        });
        if (documentTypeDetailArrayList == null) {
            documentTypeDetailArrayList = new ArrayList<>();
        }
        DocumentTypeDetail documentTypeDetail = new DocumentTypeDetail();
        documentTypeDetail.setId(-1);
        documentTypeDetail.setDocumentType("Select doc type");
        documentTypeDetail.setShowType("");
        documentTypeDetailArrayList.add(documentTypeDetail);
        for (int i = 0; i < LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getDocumentTypeDetails().size(); i++) {
            if (LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getDocumentTypeDetails().get(i).getShowType().equalsIgnoreCase("M") || LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getDocumentTypeDetails().get(i).getShowType().equalsIgnoreCase("B")) {
                documentTypeDetailArrayList.add(LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getDocumentTypeDetails().get(i));
            }
        }
        mSpinnerDocTypeArrayAdapter = new SpinnerArrayAdapter(APBCapUploadVerification.this, documentTypeDetailArrayList);
        spinner_select_doc_type.setAdapter(mSpinnerDocTypeArrayAdapter);
        spinner_select_doc_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                switch (position) {
                    case 0:
                        selectedDocTypeDetail = null;
                        break;
                    default:
                        selectedDocTypeDetail = documentTypeDetailArrayList.get(position);
                        break;


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void updateScannedOrderIDToServer() {
        JSONObject params = new JSONObject();
        try {
            params.put("mobileNo", edit_text_mobile_no.getText().toString().trim());
            params.put("documentType", selectedDocTypeDetail.getId());
        } catch (JSONException e) {
            e.printStackTrace();
            GlobalData.getApp().dismissProgressDialog();
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "please try again", GlobalData.ColorType.ERROR);
            return;
        }
        GlobalData.getApp().showProgressDialog(APBCapUploadVerification.this, "Initiating", "Please wait");
        MyVolleyRequestParsing.jsonPostWithHeader(Constants.CheckMobile, LocalPersistenceManager.getLocalPersistenceManager().getHeader(), params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {
                            try {
                                if (MyVolleyRequestParsing.response_code == 200) {
                                    Gson mGson = new Gson();
                                    StatusDAO statusDAO = mGson.fromJson(response, StatusDAO.class);
                                    if (statusDAO != null && statusDAO.getStatusCode() == 200) {
                                        String strSelectedDocTypeDetail = mGson.toJson(selectedDocTypeDetail);
                                        Intent chIntent = new Intent(APBCapUploadVerification.this, CameraXImageCapture.class);
                                        chIntent.putExtra("mobileNo", edit_text_mobile_no.getText().toString().trim());
                                        chIntent.putExtra("SelectedDocTypeDetail", strSelectedDocTypeDetail);
                                        intentActivityResultLauncher.launch(chIntent);
                                    } else if (statusDAO != null && statusDAO.getStatusCode() == 201) {
                                        String strSelectedDocTypeDetail = mGson.toJson(selectedDocTypeDetail);
                                        Intent chIntent = new Intent(APBCapUploadVerification.this, CameraXImageCapture.class);
                                        chIntent.putExtra("mobileNo", edit_text_mobile_no.getText().toString().trim());
                                        chIntent.putExtra("SelectedDocTypeDetail", strSelectedDocTypeDetail);
                                        intentActivityResultLauncher.launch(chIntent);
                                    } else if (statusDAO != null && statusDAO.getStatusCode() == 202) {
                                        reset();
                                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), statusDAO.getStatusMessage(), GlobalData.ColorType.SUCCESS);
                                        GlobalData.getApp().showAlertDialogWithOk(APBCapUploadVerification.this, "Alert", statusDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                                GlobalData.getApp().dismissProgressDialog();
                                            }
                                        });

                                    } else if (statusDAO != null && statusDAO.getStatusCode() == 203) {
                                        reset();
                                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), statusDAO.getStatusMessage(), GlobalData.ColorType.SUCCESS);
                                        GlobalData.getApp().showAlertDialogWithOk(APBCapUploadVerification.this, "Alert", statusDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();

                                            }
                                        });


                                    } else if (statusDAO != null && statusDAO.getStatusCode() == 204) {
                                        reset();
                                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), statusDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                    } else if (statusDAO != null && statusDAO.getStatusCode() == 500) {
                                        reset();
                                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);

                                    } else {
                                        reset();
                                        GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Please try again", GlobalData.ColorType.ERROR);
                                    }

                                } else if (MyVolleyRequestParsing.response_code == 401) {
                                    GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                            Intent myintent = new Intent(APBCapUploadVerification.this, APBLoginActivity.class);
                                            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            if (myintent != null) {
                                                startActivity(myintent);
                                                finish();
                                            }
                                        }
                                    }, 2000);
                                } else {
                                    GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                            Intent myintent = new Intent(APBCapUploadVerification.this, APBLoginActivity.class);
                                            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            if (myintent != null) {
                                                startActivity(myintent);
                                                finish();
                                            }
                                        }
                                    }, 2000);
                                }


                            } catch (Exception e) {
                                reset();
                                GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                e.printStackTrace();

                            }


                        }


                    }
                });

    }

    private void reset() {
        edit_text_mobile_no.setText("");
        edit_text_mobile_no_reenter.setText("");
        spinner_select_doc_type.setSelection(0);

    }

    private boolean isAllFieldValidated() throws Exception {
        PhoneNumberUtil numberUtil = PhoneNumberUtil.createInstance(this);

        boolean isAllFieldValid = true;
        if (edit_text_mobile_no.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Enter mobile number", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (edit_text_mobile_no.getText().toString().trim().length() != 10) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (!numberUtil.isValidNumber(numberUtil.parse(edit_text_mobile_no.getText().toString().trim(), "IN"))) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (edit_text_mobile_no_reenter.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Re-enter mobile number", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (edit_text_mobile_no_reenter.getText().toString().trim().length() != 10) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Re-entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (!numberUtil.isValidNumber(numberUtil.parse(edit_text_mobile_no_reenter.getText().toString().trim(), "IN"))) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Re-entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (!edit_text_mobile_no.getText().toString().trim().equals(edit_text_mobile_no_reenter.getText().toString().trim())) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Mobile number and Re-entered mobile number did not match", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (selectedDocTypeDetail == null) {
            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), "Select doc type", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        }


        return isAllFieldValid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        super.onStop();
    }

    ActivityResultLauncher<Intent> intentActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        if (result.getData() != null && result.getData().hasExtra("message")) {
                            String stringArrayList = result.getData().getStringExtra("message");
                            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), stringArrayList, GlobalData.ColorType.SUCCESS);

                        }

                    } else if (result.getResultCode() == Activity.RESULT_CANCELED) {
                        if (result.getData() != null && result.getData().hasExtra("message")) {
                            String stringArrayList = result.getData().getStringExtra("message");
                            GlobalData.showSnackbar(APBCapUploadVerification.this, findViewById(android.R.id.content), stringArrayList, GlobalData.ColorType.ERROR);

                        }
                    }
                }
            });
}
