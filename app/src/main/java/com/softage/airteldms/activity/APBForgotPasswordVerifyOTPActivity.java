package com.softage.airteldms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.utils.LocalPersistenceManager;

public class APBForgotPasswordVerifyOTPActivity extends AppCompatActivity {
    private AppCompatEditText editTextUserId;
    private AppCompatEditText editTextNewPassword;
    private AppCompatEditText editTextConfirmPassword;
    private AppCompatEditText editTextOtp;
    private MaterialButton btnCreatePassword;
    private String TAG = APBForgotPasswordVerifyOTPActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apb_forgot_password_verify_otp_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText("Create New Password");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        editTextUserId = (AppCompatEditText) findViewById(R.id.edit_text_user_id);
        editTextNewPassword = (AppCompatEditText) findViewById(R.id.editText_new_password);
        editTextConfirmPassword = (AppCompatEditText) findViewById(R.id.editText_confirm_password);
        editTextOtp = (AppCompatEditText) findViewById(R.id.editText_otp);
        btnCreatePassword = (MaterialButton) findViewById(R.id.btn_create_password);
        btnCreatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkConnection.isNetworkAvailable(APBForgotPasswordVerifyOTPActivity.this)) {
                    if (isAllFieldValidated()) {
                        // sendVerificationData();
                        LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                        Intent myintent = new Intent(APBForgotPasswordVerifyOTPActivity.this, APBLoginActivity.class);
                        myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        if (myintent != null) {
                            startActivity(myintent);
                            finish();
                        }

                    }
                } else {
                    GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        super.onStop();
    }

    /**
     * Validating all field that was entered by Agent
     *
     * @return
     */
    private boolean isAllFieldValidated() {
        boolean isAllFieldValid = true;
        if (editTextUserId.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Enter user ID", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextNewPassword.getText().toString().equals("")) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Enter new password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextConfirmPassword.getText().toString().equals("")) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Enter confirmed password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextNewPassword.getText().toString().length() <= 7) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "New password length should be between 8-10 characters", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextConfirmPassword.getText().toString().length() <= 7) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Confirm password length should be between 8-10 characters", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextOtp.getText().toString().equals("")) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Enter OTP", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextOtp.getText().toString().length() < 6) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "OTP length must be 4", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (!editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "New and confirm password does not match", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        }
        return isAllFieldValid;
    }

   /* private void sendVerificationData() {


        JSONObject params = new JSONObject();
        try {
            params.put("userName", editTextUserId.getText().toString().trim());
            params.put("otp", editTextOtp.getText().toString().trim());
            params.put("newPassword", editTextNewPassword.getText().toString().trim());
        } catch (JSONException e) {
            return;
        }
        GlobalData.getApp().showProgressDialog(APBForgotPasswordVerifyOTPActivity.this, "Initiating", "Please wait");
        MyVolleyRequestParsing.jsonPost(Constants.ResetForgotPassword, params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {


                            if (MyVolleyRequestParsing.response_code == 200 || MyVolleyRequestParsing.response_code == 201) {
                                Gson mGson = new Gson();
                                ForgotPasswordOTPVerifyDAO agentLocationDAO = mGson.fromJson(response, ForgotPasswordOTPVerifyDAO.class);
                                try {
                                    if (agentLocationDAO != null && (agentLocationDAO.getAdditionalStatusCode() == 200)) {
                                        GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), agentLocationDAO.getMessage(), GlobalData.ColorType.SUCCESS);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                LocalPersistenceManager.getLocalPersistenceManager().setRunnerLoginStatus(false);
                                                Intent myintent = new Intent(APBForgotPasswordVerifyOTPActivity.this, APBLoginActivity.class);
                                                myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                if (myintent != null) {
                                                    startActivity(myintent);
                                                    finish();
                                                }
                                            }
                                        }, 2000);

                                    } else if (agentLocationDAO != null && !agentLocationDAO.getMessage().equals("")) {
                                        GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), agentLocationDAO.getMessage().toString(), GlobalData.ColorType.ERROR);
                                    } else {
                                        GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                                    }


                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                                }
                            } else {
                                GlobalData.showSnackbar(APBForgotPasswordVerifyOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                            }


                        }
                    }
                });
    }*/
}
