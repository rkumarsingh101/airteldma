package com.softage.airteldms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softage.airteldms.R;
import com.softage.airteldms.adaptors.APBCapSearchAdaptor;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.documentDetailsDAO.DocumentDetailsDAO;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.LocalPersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;

public class APBCapSearchActivity extends AppCompatActivity {
    private String TAG = APBCapSearchActivity.class.getSimpleName();
    private AppCompatEditText edit_text_mobile_no;
    private AppCompatImageView image_view_search;
    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private AppCompatTextView txt_view_mobile_no;
    private AppCompatTextView txt_view_doc_type;
    private AppCompatTextView txt_view_status;
    private AppCompatTextView txt_view_uploaded_date_and_time;
    private AppCompatTextView txt_view_image_count;
    private AppCompatTextView txt_view_rejection_reason;
    private AppCompatTextView txt_view_remarks;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apb_cap_search_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(getResources().getString(R.string.nav_item_uploaded_cap_search));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        edit_text_mobile_no = (AppCompatEditText) findViewById(R.id.edit_text_mobile_no);
        image_view_search = (AppCompatImageView) findViewById(R.id.image_view_search);
        txt_view_mobile_no = (AppCompatTextView) findViewById(R.id.txt_view_mobile_no);
        txt_view_doc_type = (AppCompatTextView) findViewById(R.id.txt_view_doc_type);
        txt_view_status = (AppCompatTextView) findViewById(R.id.txt_view_status);
        txt_view_uploaded_date_and_time = (AppCompatTextView) findViewById(R.id.txt_view_uploaded_date_and_time);
        txt_view_image_count = (AppCompatTextView) findViewById(R.id.txt_view_image_count);
        txt_view_rejection_reason = (AppCompatTextView) findViewById(R.id.txt_view_rejection_reason);
        txt_view_remarks = (AppCompatTextView) findViewById(R.id.txt_view_remarks);
        image_view_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.getApp().hideKeyboardFromActivity(APBCapSearchActivity.this);
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CheckNetworkConnection.isNetworkAvailable(APBCapSearchActivity.this)) {
                    try {
                        if (isAllFiledValidated()) {
                            getSearchList();
                        } else {
                            resetValue();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                    }
                } else {
                    GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }
            }
        });

        resetValue();
    }

    /**
     * Fetching all leads from the server that are  done by agent
     */
    private void getSearchList() {
        resetValue();
        GlobalData.getApp().showProgressDialog(APBCapSearchActivity.this, "Initiating", "Please wait");
        MyVolleyRequestParsing.getWithHeader(Constants.CAFSearch + "/" + edit_text_mobile_no.getText().toString().trim(), LocalPersistenceManager.getLocalPersistenceManager().getHeader(),
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response_string) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (APBCapSearchActivity.this != null) {
                            if (response_string.equals("")) {
                                GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);


                            } else {
                                try {
                                    Gson mGson = new Gson();
                                    DocumentDetailsDAO fshSearchOrderDAO = mGson.fromJson(new String(response_string), DocumentDetailsDAO.class);

                                    if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 200) {
                                        if (fshSearchOrderDAO != null && fshSearchOrderDAO.getDocumentDetails() != null) {
                                            setValue(fshSearchOrderDAO);

                                        } else {
                                            resetValue();
                                            GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);

                                        }
                                    } else if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 201) {
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);
                                        resetValue();

                                    } else if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 202) {
                                        resetValue();
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);
                                    } else if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 203) {
                                        resetValue();
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);
                                    } else if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 204) {
                                        resetValue();
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);
                                    } else if (fshSearchOrderDAO != null && fshSearchOrderDAO.getStatusCode() == 205) {
                                        resetValue();
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.data_not_available), GlobalData.ColorType.ERROR);
                                    } else {
                                        resetValue();
                                        GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);

                                    }


                                } catch (Exception e) {
                                    resetValue();
                                    GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                    e.printStackTrace();

                                }


                            }
                        }
                    }
                });
    }

    private boolean isAllFiledValidated() throws Exception {
        PhoneNumberUtil numberUtil = PhoneNumberUtil.createInstance(this);
        boolean isAllFiledValidated = true;
        if (edit_text_mobile_no.getText().toString().trim().equalsIgnoreCase("")) {
            GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), "First enter mobile number", GlobalData.ColorType.ERROR);
            isAllFiledValidated = false;
        } else if (edit_text_mobile_no.getText().toString().trim().length() != 10) {
            GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), "Entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFiledValidated = false;
        } else if (!numberUtil.isValidNumber(numberUtil.parse(edit_text_mobile_no.getText().toString().trim(), "IN"))) {
            GlobalData.showSnackbar(APBCapSearchActivity.this, findViewById(android.R.id.content), "Entered mobile number is not valid", GlobalData.ColorType.ERROR);
            isAllFiledValidated = false;
        }
        return isAllFiledValidated;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        super.onStop();
    }

    private void setValue(DocumentDetailsDAO fshSearchOrderDAO) {
        txt_view_mobile_no.setText("" + fshSearchOrderDAO.getDocumentDetails().getMobileNo());
        txt_view_doc_type.setText("" + fshSearchOrderDAO.getDocumentDetails().getDocumentType());
        txt_view_status.setText("" + fshSearchOrderDAO.getDocumentDetails().getStatus());
        txt_view_uploaded_date_and_time.setText("" + fshSearchOrderDAO.getDocumentDetails().getCreatedDate());
        txt_view_image_count.setText("" + fshSearchOrderDAO.getDocumentDetails().getImageCount());
        StringBuilder formattedRejectionReason = new StringBuilder();
        formattedRejectionReason.append("");
        if (fshSearchOrderDAO.getDocumentDetails() != null && fshSearchOrderDAO.getDocumentDetails().getRejectionReason() == null) {
            List<String> rejectionReason = new ArrayList<>();
            fshSearchOrderDAO.getDocumentDetails().setRejectionReason(rejectionReason);
        }
        for (int i = 0; i < fshSearchOrderDAO.getDocumentDetails().getRejectionReason().size(); i++) {
            formattedRejectionReason.append((i + 1) + ". " + fshSearchOrderDAO.getDocumentDetails().getRejectionReason().get(i));
            if (i != fshSearchOrderDAO.getDocumentDetails().getRejectionReason().size() - 1) {
                formattedRejectionReason.append("\n\n");
            }

        }


        txt_view_rejection_reason.setText(formattedRejectionReason.toString().equals("") ? "NA" : formattedRejectionReason);
        txt_view_remarks.setText((fshSearchOrderDAO.getDocumentDetails().getRemarks() == null || fshSearchOrderDAO.getDocumentDetails().getRemarks().equals("")) ? "NA" : fshSearchOrderDAO.getDocumentDetails().getRemarks());
    }

    private void resetValue() {
        txt_view_mobile_no.setText("NA");
        txt_view_doc_type.setText("NA");
        txt_view_status.setText("NA");
        txt_view_uploaded_date_and_time.setText("NA");
        txt_view_image_count.setText("NA");
        txt_view_rejection_reason.setText("NA");
        txt_view_remarks.setText("NA");
    }
}
