package com.softage.airteldms.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.google.gson.Gson;
import com.scottyab.rootbeer.RootBeer;
import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.AppConstants;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.firebaseNotification.MyFirebaseMessagingService;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.LocalPersistenceManager;
import com.softage.airteldms.utils.MessagePersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class APBSplashScreen extends AppCompatActivity {
    private TextView txt_view_app_version;
    private Handler mHandler;
    private static final String TAG = APBSplashScreen.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();// hide the title bar
        setContentView(R.layout.apb_splash_screen);
        txt_view_app_version = (TextView) findViewById(R.id.txt_view_app_version);
        txt_view_app_version.setText("Version :" + BuildConfig.VERSION_NAME);
        String device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        LocalPersistenceManager.getLocalPersistenceManager().setDeviceID(device_id);
        /*mHandler = new Handler();
        if (mHandler != null) {
            mHandler.postDelayed(mRunnable, 1500);
        }*/

        if (LocalPersistenceManager.getLocalPersistenceManager().getFCMRegistrationID().equals("")) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(MyFirebaseMessagingService.REGISTRATION_COMPLETE));
        } else {
            if (BuildConfig.LOG)
                Log.e("onResume", "onResume Called");
            mHandler = new Handler();
            if (mHandler != null) {
                mHandler.postDelayed(mRunnable, 3000);
            }
        }

        FirebaseInstallations.getInstance().getToken(false).addOnCompleteListener(new OnCompleteListener<InstallationTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<InstallationTokenResult> task) {
                if (!task.isSuccessful()) {
                    if (BuildConfig.LOG)
                        Log.e(TAG, "getInstanceId failed", task.getException());
                } else {
                    String token = task.getResult().getToken();
                    if (BuildConfig.LOG)
                        Log.e("refreshedToken", "LastLapSplashScreen :" + token);
                    if (LocalPersistenceManager.getLocalPersistenceManager().getFCMRegistrationID().equals("")) {
                        LocalBroadcastManager.getInstance(APBSplashScreen.this).unregisterReceiver(mRegistrationBroadcastReceiver);
                        LocalPersistenceManager.getLocalPersistenceManager().setFCMRegistrationID(token);
                        if (mHandler != null) {
                            mHandler.removeCallbacks(mRunnable);
                        }
                        if (CheckNetworkConnection.isNetworkAvailable(APBSplashScreen.this)) {
                            //10-08-2021 check device is rooted or not
                            RootBeer rootBeer = new RootBeer(APBSplashScreen.this);
                            if (rootBeer.isRooted()) {
                                retryDialog("Error", "Your device is rooted, please change your device");
                            } else {
                                getAppVersionFromServer();
                            }

                        } else {
                            GlobalData.showSnackbar(APBSplashScreen.this, findViewById(android.R.id.content), MessagePersistenceManager.getMessagePersistenceManager().noInternetConnectionMessage(APBSplashScreen.this), GlobalData.ColorType.ERROR);
                            retryDialog("Error", MessagePersistenceManager.getMessagePersistenceManager().noInternetConnectionMessage(APBSplashScreen.this));
                        }
                    }
                }

            }
        });


    }


    BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MyFirebaseMessagingService.REGISTRATION_COMPLETE)) {
                if (BuildConfig.LOG)
                    Log.e("onReceive", "onReceive Called");
                mHandler = new Handler();
                if (mHandler != null) {
                    mHandler.postDelayed(mRunnable, 3000);
                }
            }
        }
    };
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (CheckNetworkConnection.isNetworkAvailable(APBSplashScreen.this)) {
               /* if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }*/
                //10-08-2021 check device is rooted or not
                RootBeer rootBeer = new RootBeer(APBSplashScreen.this);
                if (rootBeer.isRooted()) {
                    retryDialog("Error", "Your device is rooted, please change your device");
                } else {
                    getAppVersionFromServer();
                }

            } else {
                GlobalData.showSnackbar(APBSplashScreen.this, findViewById(android.R.id.content), MessagePersistenceManager.getMessagePersistenceManager().noInternetConnectionMessage(APBSplashScreen.this), GlobalData.ColorType.ERROR);
                retryDialog("Error", MessagePersistenceManager.getMessagePersistenceManager().noInternetConnectionMessage(APBSplashScreen.this));
            }


        }
    };

    private void retryDialog(String title, String Message) {
        if (!isFinishing()) {
            GlobalData.getApp().showAlertDialogWithOk(this, title, Message, "Retry", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    mHandler = new Handler();
                    if (mHandler != null) {
                        mHandler.postDelayed(mRunnable, 3000);
                    }
                }
            });
        }
    }

    private void getAppVersionFromServer() {
        MyVolleyRequestParsing.get(Constants.GetApplicationVersion,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response_string) {
                        if (APBSplashScreen.this != null) {
                            if (response_string.equals("")) {
                                retryDialog("Alert", "Please try again");
                            } else {
                                if (response_string != null && !response_string.equalsIgnoreCase("") && !response_string.equalsIgnoreCase(null)) {
                                    try {
                                        if (new JSONObject(response_string).get("Version").toString().equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                            Intent intent;
                                            if (EasyPermissions.hasPermissions(APBSplashScreen.this, AppConstants.app_permission_parems)) {
                                                if (LocalPersistenceManager.getLocalPersistenceManager().getRunnerLoginStatus()) {
                                                    intent = new Intent(APBSplashScreen.this, APBRunnerMainActivity.class);
                                                } else {
                                                    intent = new Intent(APBSplashScreen.this, APBLoginActivity.class);
                                                }
                                            } else {
                                                LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                                intent = new Intent(APBSplashScreen.this, APBLoginActivity.class);
                                            }
                                            if (intent != null) {
                                                startActivity(intent);
                                                finish();
                                            }
                                        } else {
                                            updateApplicationDialog("Alert", "Please, update your app to new version");

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    retryDialog("Alert", "Please try again");
                                }


                            }

                        }
                    }
                });


    }

   /* private void getAppVersionFromServer() {
        MyVolleyRequestParsing.get(Constants.GetApplicationVersion,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        if (APBSplashScreen.this != null) {
                            if (response.equals("")) {
                                retryDialog("Alert", "Please try again");
                            } else {
                                Gson mGson = new Gson();

                                GetApplicationInfoDAO getApplicationInfoDAO = mGson.fromJson(response, GetApplicationInfoDAO.class);
                                if (getApplicationInfoDAO != null) {

                                    GlobalData.getApp().initializeFirebase(getApplicationInfoDAO);
                                    if (getApplicationInfoDAO.getVersion().equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                        FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(new OnCompleteListener<InstallationTokenResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<InstallationTokenResult> task) {
                                                if (!task.isSuccessful()) {
                                                    if (BuildConfig.LOG)
                                                        Log.e(TAG, "getInstanceId failed", task.getException());
                                                } else {
                                                    String token = task.getResult().getToken();
                                                    if (BuildConfig.LOG)
                                                        Log.e("refreshedToken", "LastLapSplashScreen :" + token);
                                                    LocalPersistenceManager.getLocalPersistenceManager().setFCMRegistrationID(token);
                                                    if (mHandler != null) {
                                                        mHandler.removeCallbacks(mRunnable);
                                                    }
                                                    //10-08-2021 check device is rooted or not

                                                    Intent intent;
                                                    if (EasyPermissions.hasPermissions(APBSplashScreen.this, AppConstants.app_permission_parems)) {
                                                        if (LocalPersistenceManager.getLocalPersistenceManager().getRunnerLoginStatus()) {
                                                            intent = new Intent(APBSplashScreen.this, APBRunnerMainActivity.class);
                                                        } else {
                                                            intent = new Intent(APBSplashScreen.this, APBLoginActivity.class);
                                                        }
                                                    } else {
                                                        LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                                        intent = new Intent(APBSplashScreen.this, APBLoginActivity.class);
                                                    }
                                                    if (intent != null) {
                                                        startActivity(intent);
                                                        finish();
                                                    }


                                                }

                                            }


                                        });

                                    } else {
                                        updateApplicationDialog("Alert", "Please, update your app to new version");

                                    }
                                } else {
                                    retryDialog("Alert", "Please try again");
                                }


                            }

                        }
                    }
                });


    }*/


    @Override
    public void onBackPressed() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        super.onBackPressed();
    }

    public void updateApplicationDialog(String title, String message) {
        if (!isFinishing()) {

            GlobalData.getApp().showAlertDialogWithOk(this, title, message, "Update", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });


        }
    }

    /**
     * Lifecycle Method of Activity
     */
    @Override
    protected void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            LocalBroadcastManager.getInstance(APBSplashScreen.this).unregisterReceiver(mRegistrationBroadcastReceiver);
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
