package com.softage.airteldms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.View;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.latLngAddressDAO.LatLngAddressDAO;
import com.softage.airteldms.dao.tokenDAO.TokenDAO;
import com.softage.airteldms.dao.userDetailsDAO.DocumentTypeDetail;
import com.softage.airteldms.dao.userDetailsDAO.UserDetailsDAO;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.GeoLocationManager;
import com.softage.airteldms.utils.LocalPersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class APBLoginActivity extends APBBaseActivity {
    private MaterialButton btn_login;
    private AppCompatTextView txt_view_forgetPassword;

    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private AppCompatEditText edit_text_user_id;
    private AppCompatEditText edit_text_login_password;
    private String TAG = APBLoginActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();// hide the title bar
        setContentView(R.layout.apb_login_activity);
        btn_login = (MaterialButton) findViewById(R.id.btn_login);
        edit_text_user_id = (AppCompatEditText) findViewById(R.id.edit_text_user_id);
        edit_text_login_password = (AppCompatEditText) findViewById(R.id.editText_login_password);
        txt_view_forgetPassword = (AppCompatTextView) findViewById(R.id.txt_view_forgetPassword);
        GlobalData.getApp().hideSoftKeyboard(this);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CheckNetworkConnection.isNetworkAvailable(APBLoginActivity.this)) {
                    if (GlobalData.getApp().isHighAccuracyLocationModeEnable()) {
                        if (isAllFieldValidated())
                            validateUser();
                    } else {
                        openLocationModeSettingDialog("Alert", "Please select location mode High accuracy");
                    }
                } else {
                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }


            }
        });

        txt_view_forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent intent = new Intent(APBLoginActivity.this, APBForgotPasswordRequestOTPActivity.class);
                startActivity(intent);
            }
        });
        edit_text_user_id.setText(LocalPersistenceManager.getLocalPersistenceManager().getUserCode());
        edit_text_login_password.setText(LocalPersistenceManager.getLocalPersistenceManager().getUserPassword());
    }

    public void openLocationModeSettingDialog(String title, String message) {
        if (!isFinishing()) {
            GlobalData.getApp().showAlertDialogWithOk(this, title, message, "Update", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });


        }
    }

    private void validateUser() {
        GlobalData.getApp().showProgressDialog(APBLoginActivity.this, "Authenticating", "Please wait");
        JSONObject params = new JSONObject();
        try {
            params.put("userName", edit_text_user_id.getText().toString().trim());
            params.put("password", edit_text_login_password.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        MyVolleyRequestParsing.jsonPost(Constants.Login, params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();

                        if (response.equals("")) {
                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {
                            try {
                                if (MyVolleyRequestParsing.response_code == 200) {
                                    Gson mGson = new Gson();
                                    TokenDAO mTokenDAO = mGson.fromJson(response, TokenDAO.class);
                                    if (mTokenDAO != null && !mTokenDAO.getToken().equalsIgnoreCase("")) {
                                        LocalPersistenceManager.getLocalPersistenceManager().setHeader(mTokenDAO.getToken());
                                        LocalPersistenceManager.getLocalPersistenceManager().setAgentRole(mTokenDAO.getRole());
                                        if (CheckNetworkConnection.isNetworkAvailable(APBLoginActivity.this)) {
                                            getUserDetail();
                                        } else {
                                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), GlobalData.ColorType.ERROR);
                                        }
                                    } else {
                                        GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Please try again", GlobalData.ColorType.ERROR);
                                    }
                                } else if (MyVolleyRequestParsing.response_code == 400) {
                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Entered User ID or password is not valid", GlobalData.ColorType.ERROR);
                                } else {
                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "User ID or password is not valid", GlobalData.ColorType.ERROR);
                                }


                            } catch (Exception e) {
                                GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                e.printStackTrace();

                            }


                        }


                    }
                });
    }

    private void getUserDetail() {
        GeoLocationManager geoLocationManager = new GeoLocationManager(this);
        geoLocationManager.getCurrentLatLngAddress(true);
        geoLocationManager.registerILatLngAddressResolver(new GeoLocationManager.ILatLngAddressResolver() {
            @Override
            public void latLngAddressResolver(int responseCode, String message, LatLngAddressDAO mLatLngAddressDAO) {
                if (responseCode == GeoLocationManager.OK) {
                    JSONObject params = new JSONObject();
                    try {
                        params.put("deviceId", LocalPersistenceManager.getLocalPersistenceManager().getDeviceID());
                        params.put("firebaseId", LocalPersistenceManager.getLocalPersistenceManager().getFCMRegistrationID());
                        params.put("latitude", "" + mLatLngAddressDAO.getLatitude());
                        params.put("longitude", "" + mLatLngAddressDAO.getLongitude());
                        params.put("address", mLatLngAddressDAO.getAddress());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        GlobalData.getApp().dismissProgressDialog();
                        GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Your current location is not fetched, please try again", GlobalData.ColorType.ERROR);
                        return;
                    }
                    MyVolleyRequestParsing.jsonPostWithHeader(Constants.UserDetails, LocalPersistenceManager.getLocalPersistenceManager().getHeader(), params,
                            TAG, new IVolleyResponse() {
                                @Override
                                public void response(String response) {

                                    GlobalData.getApp().dismissProgressDialog();
                                    if (response.equals("")) {
                                        GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                                    } else {
                                        try {
                                            if (MyVolleyRequestParsing.response_code == 200) {
                                                Gson mGson = new Gson();
                                                UserDetailsDAO userDetailDAO = mGson.fromJson(response, UserDetailsDAO.class);
                                                if (userDetailDAO != null && userDetailDAO.getStatusCode() == 200) {

                                                    if (LocalPersistenceManager.getLocalPersistenceManager().getAgentRole().equalsIgnoreCase("Agent")) {
                                                        if (userDetailDAO == null || userDetailDAO.getAgentDetails() == null || userDetailDAO.getAgentDetails().getDocumentTypeDetails() == null || userDetailDAO.getAgentDetails().getDocumentTypeDetails().size() == 0) {
                                                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Document type is not found, " + getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                                        } else if (userDetailDAO.getAgentDetails().getFtpDetail() == null || userDetailDAO.getAgentDetails().getFtpDetail().getFtpip() == null || userDetailDAO.getAgentDetails().getFtpDetail().getFtpip().trim().equalsIgnoreCase("") || userDetailDAO.getAgentDetails().getFtpDetail().getFtpPassword() == null || userDetailDAO.getAgentDetails().getFtpDetail().getFtpPassword().trim().equalsIgnoreCase("") || userDetailDAO.getAgentDetails().getFtpDetail().getFtpUser() == null || userDetailDAO.getAgentDetails().getFtpDetail().getFtpUser().equalsIgnoreCase("") || userDetailDAO.getAgentDetails().getFtpDetail().getFtpPort() == null) {
                                                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "SFTP detail is not found, " + getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                                        } else if (userDetailDAO.getAgentDetails().getImageCount() == null || userDetailDAO.getAgentDetails().getImageCount() == 0) {
                                                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Total image count detail is not valid, " + getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                                        } else {
                                                            LocalPersistenceManager.getLocalPersistenceManager().setRunnerLoginStatus(true);
                                                            LocalPersistenceManager.getLocalPersistenceManager().setUserCode(edit_text_user_id.getText().toString().trim());
                                                            LocalPersistenceManager.getLocalPersistenceManager().setUserPassword(edit_text_login_password.getText().toString());
                                                            Collections.sort(userDetailDAO.getAgentDetails().getDocumentTypeDetails(), new DocumentTypeComparator());
                                                            LocalPersistenceManager.getLocalPersistenceManager().setAgentDetailsDAO(userDetailDAO.getAgentDetails());
                                                            Intent intent = new Intent(APBLoginActivity.this, APBRunnerMainActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    } else {
                                                        GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Your role is not defined, " + getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                                    }

                                                } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 201) {
                                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                                } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 202) {

                                                    if (!isFinishing()) {
                                                        GlobalData.getApp().showAlertDialogWithOk(APBLoginActivity.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                GlobalData.getApp().dismissProgressDialog();

                                                            }
                                                        });
                                                    }

                                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                                } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 205) {
                                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.SUCCESS);
                                                    if (!isFinishing()) {
                                                        GlobalData.getApp().showAlertDialogWithOk(APBLoginActivity.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                GlobalData.getApp().dismissProgressDialog();

                                                            }
                                                        });
                                                    }
                                                } else {
                                                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Please try again", GlobalData.ColorType.ERROR);
                                                }


                                            } else if (MyVolleyRequestParsing.response_code == 401) {
                                                GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                                        Intent myintent = new Intent(APBLoginActivity.this, APBLoginActivity.class);
                                                        myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        if (myintent != null) {
                                                            startActivity(myintent);
                                                            finish();
                                                        }
                                                    }
                                                }, 2000);
                                            } else {
                                                GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                            }


                                        } catch (Exception e) {
                                            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                            e.printStackTrace();

                                        }


                                    }


                                }
                            });
                } else if (responseCode == GeoLocationManager.FAKE_LOCATION) {
                    GlobalData.getApp().showAlertDialogWithOk(APBLoginActivity.this, "Fake location found", "Please first remove/uninstall fake location application", "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GlobalData.getApp().dismissProgressDialog();
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                            startActivity(intent);
                        }
                    });
                } else {
                    GlobalData.getApp().dismissProgressDialog();
                    GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Your current location is not fetched, please try again", GlobalData.ColorType.ERROR);
                }

            }
        });


    }

    private class DocumentTypeComparator implements Comparator<DocumentTypeDetail> {
        @Override
        public int compare(DocumentTypeDetail o1, DocumentTypeDetail o2) {
            return o1.getDocumentType().compareTo(o2.getDocumentType());
        }
    }

    @Override
    protected void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);

        super.onStop();
    }

    private boolean isAllFieldValidated() {
        boolean isAllFieldValid = true;
        if (edit_text_user_id.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Enter user ID", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (edit_text_login_password.getText().toString().equals("")) {
            GlobalData.showSnackbar(APBLoginActivity.this, findViewById(android.R.id.content), "Enter password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        }
        return isAllFieldValid;
    }


}
