package com.softage.airteldms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.adaptors.NavigationDrawerAdapter;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.nevDrawerItem.NavDrawerItem;
import com.softage.airteldms.fragment.APBHomeFragment;
import com.softage.airteldms.fragment.APBResetPasswordFragment;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.utils.LocalPersistenceManager;
import com.softage.airteldms.utils.UtilsFragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class APBRunnerMainActivity extends AppCompatActivity implements NavigationDrawerAdapter.OnDrawerItemClick {
    private RecyclerView nav_drawer_recycler_view;
    private NavigationDrawerAdapter mNavigationDrawerAdapter;
    private List<NavDrawerItem> nevigationDrawerdata;
    private DrawerLayout drawer;
    private TextView text_view_app_version;
    private AppCompatTextView title;
    private AppCompatTextView txt_view_user_detail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apb_runner_main_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        title = (AppCompatTextView) findViewById(R.id.title);
        txt_view_user_detail = (AppCompatTextView) findViewById(R.id.txt_view_user_detail);
        text_view_app_version = (TextView) findViewById(R.id.text_view_app_version);
        text_view_app_version.setText("Version " + BuildConfig.VERSION_NAME);
        nav_drawer_recycler_view = (RecyclerView) findViewById(R.id.nav_drawer_sales_app_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        nav_drawer_recycler_view.setLayoutManager(mLayoutManager);
        nav_drawer_recycler_view.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        nav_drawer_recycler_view.setItemAnimator(new DefaultItemAnimator());
        addNavigationDrawerData();
        if (mNavigationDrawerAdapter == null) {
            mNavigationDrawerAdapter = new NavigationDrawerAdapter(APBRunnerMainActivity.this, nevigationDrawerdata);
            mNavigationDrawerAdapter.setOnDrawerItemClick(this);
            nav_drawer_recycler_view.setAdapter(mNavigationDrawerAdapter);
        } else {
            mNavigationDrawerAdapter.notifyDataSetChanged();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        UtilsFragmentTransaction.replaceFragmentTransaction(R.id.fragment_container, this, new APBHomeFragment(), null);
        getSupportActionBar().setTitle("");
        title.setText(R.string.nav_item_home);
        String userName = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getName();
        String userId = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getUserId();
        if (userId.length() > 2) {
            String first_albhapets = userId.substring(0, 2);
            first_albhapets.toUpperCase();
            userId = first_albhapets + userId.substring(2, userId.length());
        }
        txt_view_user_detail.setText(toProperCase(userName) + "(" + userId + ")");
    }

    @Override
    public void onDrawerItemClicked(int position) {
        String title = getString(R.string.app_name);
        boolean isTitleChangeRequired = false;
        switch (position) {
            case 0:
                UtilsFragmentTransaction.replaceFragmentTransaction(R.id.fragment_container, this, new APBHomeFragment(), null);
                title = "";
                isTitleChangeRequired = true;
                this.title.setText(R.string.nav_item_home);
                break;
            case 1:
                UtilsFragmentTransaction.replaceFragmentTransaction(R.id.fragment_container, this, new APBResetPasswordFragment(), null);
                title = "";
                isTitleChangeRequired = true;
                this.title.setText(R.string.nav_item_reset_password);
                break;

            case 2:
                isTitleChangeRequired = false;
                logoutDialog("Alert", "Are you sure you want to logout?");
                break;


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (isTitleChangeRequired) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void addNavigationDrawerData() {
        if (nevigationDrawerdata == null) {
            nevigationDrawerdata = new ArrayList<>();
        }
        nevigationDrawerdata.removeAll(nevigationDrawerdata);


        int image[] = {R.drawable.manage_order, R.drawable.reset_paassword, R.drawable.logout};
        String title[] = getResources().getStringArray(R.array.runner_nav_drawer_labels);
        for (int i = 0; i < image.length; i++) {
            NavDrawerItem navDrawerItem = new NavDrawerItem();
            navDrawerItem.setImage(image[i]);
            navDrawerItem.setTitle(title[i]);
            nevigationDrawerdata.add(navDrawerItem);
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
            exitDialog("Alert", "Are you sure you want to exit?");
        }
    }

    /**
     * For showing logout dialog while user logout form the application
     *
     * @param messge
     */
    private void logoutDialog(String title, String messge) {
        if (!isFinishing()) {

            GlobalData.getApp().showAlertDialogWithOkCancel(this, title, messge, "OK", "Cancel", R.color.red, R.color.green_dark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    if (CheckNetworkConnection.isNetworkAvailable(APBRunnerMainActivity.this)) {
                        LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                        Intent i1 = new Intent(APBRunnerMainActivity.this, APBLoginActivity.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i1);
                        finish();

                    } else {
                        GlobalData.showSnackbar(APBRunnerMainActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), GlobalData.ColorType.ERROR);
                    }
                }
            }, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                }
            });
        }
    }


    private void exitDialog(String title, String messge) {
        if (!isFinishing()) {

            GlobalData.getApp().showAlertDialogWithOkCancel(this, title, messge, "OK", "Cancel", R.color.red, R.color.green_dark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    finish();
                }
            }, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                }
            });
        }
    }

    private String toProperCase(String userName) {
        userName.toLowerCase();
        StringBuilder ProperCase = new StringBuilder(userName.length());
        boolean nextProperCase = true;

        for (char c : userName.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextProperCase = true;
            } else if (nextProperCase) {
                c = Character.toTitleCase(c);
                nextProperCase = false;
            }
            ProperCase.append(c);
        }
        return ProperCase.toString();
    }
}
