package com.softage.airteldms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class APBForgotPasswordRequestOTPActivity extends AppCompatActivity {
    private AppCompatEditText editTextUserId;
    private MaterialButton btnForgetPasswordGetOtp;
    private String TAG = APBForgotPasswordRequestOTPActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apb_forgot_password_request_otp_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText("Request OTP");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        editTextUserId = (AppCompatEditText) findViewById(R.id.edit_text_user_id);
        btnForgetPasswordGetOtp = (MaterialButton) findViewById(R.id.btn_forget_password_get_otp);
        btnForgetPasswordGetOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkConnection.isNetworkAvailable(APBForgotPasswordRequestOTPActivity.this)) {
                    if (isAllFieldValidated()) {
                        GlobalData.getApp().dismissProgressDialog();
                        Intent mIntent = new Intent(APBForgotPasswordRequestOTPActivity.this, APBForgotPasswordVerifyOTPActivity.class);
                        startActivity(mIntent);
                        finish();

                    }
                } else {
                    GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }
            }
        });

       // GlobalData.getApp().hideSoftKeyboard(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Validating all field that was entered by Agent
     *
     * @return
     */
    private boolean isAllFieldValidated() {
        boolean isAllFieldValid = true;
        if (editTextUserId.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), "Enter User ID", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        }

        return isAllFieldValid;
    }

    /**
     * Send mobile no and aadhaar card no for resetting password
     *//*
    private void sendVerificationData() {
        JSONObject params = new JSONObject();
        try {
            params.put("userName", editTextUserId.getText().toString().trim());
        } catch (JSONException e) {
            return;
        }
        GlobalData.getApp().showProgressDialog(APBForgotPasswordRequestOTPActivity.this, "Initiating", "Please wait");

        MyVolleyRequestParsing.jsonPost(Constants.ForgotPassword, params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {
                            if (MyVolleyRequestParsing.response_code == 200 || MyVolleyRequestParsing.response_code == 201) {
                                Gson mGson = new Gson();
                                final ForgotPasswordDAO agentLocationDAO = mGson.fromJson(response, ForgotPasswordDAO.class);

                                try {
                                    if (agentLocationDAO != null && (agentLocationDAO.getAdditionalStatusCode() == 200)) {
                                        GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), agentLocationDAO.getMessage().toString(), GlobalData.ColorType.SUCCESS);
                                        messageDialog("Confirmation", agentLocationDAO.getMessage().toString());

                                    } else if (agentLocationDAO != null && !agentLocationDAO.getMessage().equals("")) {
                                        GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), agentLocationDAO.getMessage().toString(), GlobalData.ColorType.ERROR);
                                    } else {
                                        GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                                }


                            } else {
                                GlobalData.showSnackbar(APBForgotPasswordRequestOTPActivity.this, findViewById(android.R.id.content), "Please Try Again", GlobalData.ColorType.ERROR);
                            }


                        }
                    }
                });
    }*/

    /**
     * Lifecycle Method of Activity
     */
    @Override
    protected void onStop() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        super.onStop();
    }

    private void messageDialog(String title, String Message) {
        if (!isFinishing()) {
            GlobalData.getApp().showAlertDialogWithOk(this, title, Message, "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    Intent mIntent = new Intent(APBForgotPasswordRequestOTPActivity.this, APBForgotPasswordVerifyOTPActivity.class);
                    startActivity(mIntent);
                    finish();
                }
            });
        }
    }
}
