package com.softage.airteldms.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.AppConstants;
import com.softage.airteldms.constants.GlobalData;

import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class APBBaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    private final static int ALL_APPLICATION_PERMISSION_REQUEST_CODE = 1111;
    private static final int LOCATION_SETTING_REQUEST_CODE = 0x33;
    private Disposable locationSettingDisposable;
    private String TAG = APBBaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_PROVIDER_CHANGED);
        registerReceiver(gpsSwitchStateReceiver, filter);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {

            new AppSettingsDialog.Builder(this).build().show();
        } else {
            methodForAllPermission();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        methodForAllPermission();

    }

    /**
     * Check premission is granted by user or not(For Marshmellow and above OS)
     *
     * @throws SecurityException
     */
    @AfterPermissionGranted(ALL_APPLICATION_PERMISSION_REQUEST_CODE)
    private void methodForAllPermission() throws SecurityException {
        if (EasyPermissions.hasPermissions(this, AppConstants.app_permission_parems)) {
            if (BuildConfig.LOG)
            Log.e("All Permission", "methodForAllPermission");
            if (!GlobalData.getApp().isLocationServiceEnabled()) {
                openLocationDialog();
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    EasyPermissions.requestPermissions(APBBaseActivity.this, getString(R.string.all_permission),
                            ALL_APPLICATION_PERMISSION_REQUEST_CODE, AppConstants.app_permission_parems);
                }
            }, 500);

        }
    }

    /**
     * Lifecycle Method of Activity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            methodForAllPermission();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);


    }

    private void openLocationDialog() {

        if (locationSettingDisposable != null) {
            locationSettingDisposable.dispose();
        }
        locationSettingDisposable = GlobalData.getApp().getReactiveLocationProvider()
                .checkLocationSettings(
                        new LocationSettingsRequest.Builder()
                                .setAlwaysShow(true)
                                .addLocationRequest(GlobalData.getApp().getLocationRequest())
                                .build()).subscribe(new Consumer<LocationSettingsResult>() {
                    @Override
                    public void accept(LocationSettingsResult locationSettingsResult) throws Exception {
                        Status status = locationSettingsResult.getStatus();
                        if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                            try {
                                status.startResolutionForResult(APBBaseActivity.this, LOCATION_SETTING_REQUEST_CODE);
                            } catch (IntentSender.SendIntentException th) {
                                if (BuildConfig.LOG)
                                Log.e(TAG, "Error opening settings activity.", th);
                            }
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (BuildConfig.LOG)
                        Log.e(TAG, "Error opening settings activity.", throwable);
                    }
                });


    }

    private BroadcastReceiver gpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {
                // Make an action or refresh an already managed state.

                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGpsEnabled || !isNetworkEnabled) {
                    openLocationDialog();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(gpsSwitchStateReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }
}
