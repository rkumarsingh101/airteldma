package com.softage.airteldms.firebaseNotification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.activity.APBSplashScreen;
import com.softage.airteldms.utils.LocalPersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

import static androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        if (BuildConfig.LOG)
        Log.e("refreshedToken", refreshedToken);
        backgroundSendPushNotificationSenderIDToServer(refreshedToken);
    }

    private void backgroundSendPushNotificationSenderIDToServer(final String token) {
        LocalPersistenceManager.getLocalPersistenceManager().setFCMRegistrationID(token);
        Intent registrationComplete = new Intent(MyFirebaseMessagingService.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (BuildConfig.LOG)
        Log.e(TAG, "From: " + remoteMessage.getFrom());


        if (remoteMessage == null)
            return;


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            if (BuildConfig.LOG)
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());

        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            if (BuildConfig.LOG)
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                String title = "";
                String message = "";
                try {
                    title = object.getString("title");
                    message = object.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (BuildConfig.LOG) {
                    Log.e("JSON_OBJECT", object.toString());
                    Log.e("title", title);
                    Log.e("message", message);
                }
                handleNotification(title, message);
            } catch (Exception e) {
                if (BuildConfig.LOG)
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String title, String message) {
        int importance = NotificationManager.IMPORTANCE_HIGH;
        String CHANNEL_ID = "LeadAssigningAlert";
        CharSequence CHANNEL_NAME = "Lead Assigning Alert";
        Intent notifyIntent = new Intent(MyFirebaseMessagingService.this, APBSplashScreen.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pi = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0,
                notifyIntent, 0);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID);
        notification.setContentTitle(title);
        notification.setContentText(message);
        notification.setAutoCancel(true);
        notification.setContentIntent(pi);
        notification.setShowWhen(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.setSmallIcon(R.drawable.ic_notification_icon);
            notification.setColor(getResources().getColor(R.color.colorPrimary));
        } else {
            notification.setSmallIcon(R.drawable.ic_notification_icon);
        }
        notification.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        notification.setLocalOnly(true);
        notification.setVisibility(VISIBILITY_PUBLIC);
        notification.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/iphone_mp"));

        NotificationManager mNotificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance);
            mNotificationManager2.createNotificationChannel(notificationChannel);
        }
       /* NotificationManagerCompat.from(MyJobService.this)
                .notify(new Random().nextInt(), notification.build());*/
        mNotificationManager2.notify(new Random().nextInt(), notification.build());

    }
}
