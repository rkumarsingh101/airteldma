package com.softage.airteldms.constants;


/**
 * @author SS0111 Ranjan
 * This class is used for saving base server API's
 */
public class Constants {

    //  public static final String IpAdress = "https://www.softageahm.com/AirtelPaymentBankUATAPI/api/";
    //public static final String IpAdress = "https://www.softageahm.com/AirtelPaymentBankAPI/api/";
    //public static final String IpAdress = "https://www.softageahm.com/AirtelPaymentBankTestAPI/api/";
    //public static final String IpAdress = "https://apbldms.softage.net/apbl/api/";


    //  public static final String IpAdress = "https://www.softageahm.com/AirtelPaymentBankUATAPI/api/";





    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++Testing Static URL+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++Testing Static URL+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++Live URL+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static final String IpAdress = "https://www.softageteledocm.com/AirtelPaymentBankApi/api/";//Live URL
    // public static final String IpAdress = "https://apbldms.softage.net/apbl/api/";
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static final String GetApplicationVersion = IpAdress + "application/GetApplicationVersion";
    public static final String Login = IpAdress + "Account/Login";
    public static final String ResetPassword = IpAdress + "Account/ChangePassword";

    public static final String UserDetails = IpAdress + "User/GetUserDetailAPI";
    public static final String CAFSearch = IpAdress + "Document/Search";
    public static final String CheckMobile = IpAdress + "Document/CheckMobile";
    public static final String UploadDocuments = IpAdress + "Document/UploadDocuments";
    public static final String GetFileDetailsToDownload = IpAdress + "Document/GetFileDetailsToDownload";

}
