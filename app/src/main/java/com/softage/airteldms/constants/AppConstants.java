package com.softage.airteldms.constants;

import android.Manifest;

/**
 * Class for Managing constant values, that are naver changed.
 *
 * @author SS0111 Ranjan
 */

public class AppConstants {
    public static final String PREF_KEY_HEADER = "header";
    public static final String PREF_KEY_AGENT_DETAIL = "agentDetail";
    public static final String PREF_KEY_FCM_REGISTRATION_ID = "registrationID";
    public static final String PREF_KEY_DEVICE_ID = "Device";
    public static final String PREF_KEY_USER_ROLE = "Role";
    public static final String PREF_KEY_USER_TOKEN = "UserToken";
    public static final String PREF_KEY_SUPPORT_MAIL_ID = "MailID";
    public static final String PREF_KEY_AGENT_CODE = "AgentCode";
    public static final String PREF_KEY_AGENT_PASSWORD = "AgentPassword";
    public static final String PREF_KEY_RUNNER_LOGIN_STATUS = "RunnerLoginStatus";

//App needs these permission for smooth working of application
    public final static String[] app_permission_parems = {Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};


}

