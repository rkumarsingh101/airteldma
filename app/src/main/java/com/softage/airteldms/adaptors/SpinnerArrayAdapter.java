package com.softage.airteldms.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.softage.airteldms.R;
import com.softage.airteldms.dao.userDetailsDAO.DocumentTypeDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for managing view and data that is infleted in Spinner
 *
 * @author SS0111 Ranjan
 */
public class SpinnerArrayAdapter extends ArrayAdapter<DocumentTypeDetail> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private ArrayList<DocumentTypeDetail> documentTypeDetailArrayList;

    public SpinnerArrayAdapter(@NonNull Context context,
                               @NonNull ArrayList<DocumentTypeDetail> documentTypeDetailArrayList) {
        super(context, R.layout.spinner_search_by_report_item, 0, documentTypeDetailArrayList);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        this.documentTypeDetailArrayList = documentTypeDetailArrayList;
    }

    /**
     * Lifecycle method for ArrayAdapter class
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createDropDownView(position, convertView, parent);
    }

    /**
     * Lifecycle method for ArrayAdapter class
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_search_by_report_item, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.spinnerItem = (TextView) convertView.findViewById(R.id.txt_view_spinner_search_by_items);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }


        mViewHolder.spinnerItem.setText(documentTypeDetailArrayList.get(position).getDocumentType());

        return convertView;
    }

    private View createDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_drop_down_item, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.spinnerItem = (TextView) convertView.findViewById(R.id.txt_view_spinner_drop_down_item);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }


        mViewHolder.spinnerItem.setText(documentTypeDetailArrayList.get(position).getDocumentType());

        return convertView;
    }

    static class ViewHolder {
        TextView spinnerItem;
    }


}
