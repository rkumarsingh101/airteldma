package com.softage.airteldms.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.softage.airteldms.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class AllCapturedImagePreviewAdaptor extends RecyclerView.Adapter {
    private Context context;
    private LayoutInflater inflater;
    private List<File> files;


    public AllCapturedImagePreviewAdaptor(Context context, List<File> files) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.files = files;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.all_captured_image_preview_list_item, parent, false);
        AllCapturedImagePreviewHolder holder = new AllCapturedImagePreviewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Picasso.get()
                .load(files.get(position))
                .into(((AllCapturedImagePreviewHolder) holder).image_view_captured_image);
        ((AllCapturedImagePreviewHolder) holder).txt_view_image_count.setText("" + (position + 1) + "/" + files.size());


    }


    @Override
    public int getItemCount() {
        return files.size();
    }

    class AllCapturedImagePreviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatImageView image_view_captured_image;
        private AppCompatTextView txt_view_image_count;
        private AppCompatImageView image_view_captured_image_delete;


        public AllCapturedImagePreviewHolder(View view) {
            super(view);
            image_view_captured_image = (AppCompatImageView) view.findViewById(R.id.image_view_captured_image);
            txt_view_image_count = (AppCompatTextView) view.findViewById(R.id.txt_view_image_count);
            image_view_captured_image_delete = (AppCompatImageView) view.findViewById(R.id.image_view_captured_image_delete);
            image_view_captured_image_delete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_view_captured_image_delete:
                    if (onCapturedImageDeleteClicked != null) {
                        onCapturedImageDeleteClicked.onCapturedImageDeleteClicked(getAdapterPosition());
                    }
                    break;
            }
        }
    }

    public interface OnCapturedImageDeleteClicked {
        void onCapturedImageDeleteClicked(int position);
    }

    private OnCapturedImageDeleteClicked onCapturedImageDeleteClicked;

    public void setOnCapturedImageDeleteClicked(OnCapturedImageDeleteClicked onCapturedImageDeleteClicked) {
        this.onCapturedImageDeleteClicked = onCapturedImageDeleteClicked;
    }


}
