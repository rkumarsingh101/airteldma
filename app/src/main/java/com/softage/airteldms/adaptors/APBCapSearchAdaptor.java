package com.softage.airteldms.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.softage.airteldms.R;


public class APBCapSearchAdaptor extends RecyclerView.Adapter {
    Context context;
    LayoutInflater inflater;


    public APBCapSearchAdaptor(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.apb_cap_search_list_item, parent, false);
        ReportViewHolder holder = new ReportViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((ReportViewHolder) holder).txt_view_mobile_no.setText("");
        ((ReportViewHolder) holder).txt_view_doc_type.setText("");
        ((ReportViewHolder) holder).txt_view_status.setText("");
        ((ReportViewHolder) holder).txt_view_uploaded_date_and_time.setText("");
    }


    @Override
    public int getItemCount() {
        return 50;
    }

    class ReportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatTextView txt_view_mobile_no;
        private AppCompatTextView txt_view_doc_type;
        private AppCompatTextView txt_view_status;
        private AppCompatTextView txt_view_uploaded_date_and_time;

        public ReportViewHolder(View view) {
            super(view);
            txt_view_mobile_no = (AppCompatTextView) itemView.findViewById(R.id.txt_view_mobile_no);
            txt_view_doc_type = (AppCompatTextView) itemView.findViewById(R.id.txt_view_doc_type);
            txt_view_status = (AppCompatTextView) itemView.findViewById(R.id.txt_view_status);
            txt_view_uploaded_date_and_time = (AppCompatTextView) itemView.findViewById(R.id.txt_view_uploaded_date_and_time);
        }

        @Override
        public void onClick(View v) {
        }
    }


}
