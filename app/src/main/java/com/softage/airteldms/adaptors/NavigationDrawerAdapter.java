package com.softage.airteldms.adaptors;

/**
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.softage.airteldms.R;
import com.softage.airteldms.dao.nevDrawerItem.NavDrawerItem;

import java.util.Collections;
import java.util.List;

/**
 * Class for managing view and data that is infleted in RecycleView
 *
 * @author SS0111 Ranjan
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.NavigationDrawerViewHolder> {
    List<NavDrawerItem> nevigationDrawerdata = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> nevigationDrawerdata) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.nevigationDrawerdata = nevigationDrawerdata;
    }

    public void delete(int position) {
        nevigationDrawerdata.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Lifecycle method for RecyclerView.Adapter
     *
     * @param parent
     * @param viewType
     * @return
     */

    @Override
    public NavigationDrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        NavigationDrawerViewHolder holder = new NavigationDrawerViewHolder(view);
        return holder;
    }

    /**
     * Lifecycle method for RecyclerView.Adapter
     *
     * @param holder
     * @param position
     */

    @Override
    public void onBindViewHolder(NavigationDrawerViewHolder holder, int position) {
        NavDrawerItem current = nevigationDrawerdata.get(position);
        holder.title.setText(current.getTitle());
        holder.imageicon.setImageResource(current.getImage());
    }

    /**
     * Lifecycle method for RecyclerView.Adapter
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return nevigationDrawerdata.size();
    }


    class NavigationDrawerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;

        ImageView imageicon;

        public NavigationDrawerViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.text_view_nav_drawer_title);
            imageicon = (ImageView) itemView.findViewById(R.id.imageView_nav_drawer_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onDrawerItemClick != null) {
                onDrawerItemClick.onDrawerItemClicked(getAdapterPosition());
            }
        }
    }

    public interface OnDrawerItemClick {
        void onDrawerItemClicked(int position);
    }

    private OnDrawerItemClick onDrawerItemClick;

    public void setOnDrawerItemClick(OnDrawerItemClick onDrawerItemClick) {
        this.onDrawerItemClick = onDrawerItemClick;
    }
}
