
package com.softage.airteldms.dao.forgotPasswordOTPVerifyDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotPasswordOTPVerifyDAO {

    @SerializedName("httpStatus")
    @Expose
    private Integer httpStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("additionalStatusCode")
    @Expose
    private Integer additionalStatusCode;

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAdditionalStatusCode() {
        return additionalStatusCode;
    }

    public void setAdditionalStatusCode(Integer additionalStatusCode) {
        this.additionalStatusCode = additionalStatusCode;
    }

}
