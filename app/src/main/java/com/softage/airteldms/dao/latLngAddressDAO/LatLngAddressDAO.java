package com.softage.airteldms.dao.latLngAddressDAO;

/**
 * Created by SS0111 on 22-Mar-18.
 */

public class LatLngAddressDAO {
    double latitude;
    private String address;
    double longitude;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


}
