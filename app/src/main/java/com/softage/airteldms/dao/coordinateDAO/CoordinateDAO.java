package com.softage.airteldms.dao.coordinateDAO;

import java.util.Comparator;

/**
 * Created by SS0111 on 22-Mar-18.
 */

public class CoordinateDAO {
    double customerLatitude;
    private String agentAddress;
    double customerLongitude;
    double agentLongitude;
    private String customerAddress;
    double distance;

    public String getAgentAddress() {
        return agentAddress;
    }

    public void setAgentAddress(String agentAddress) {
        this.agentAddress = agentAddress;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }


    public double getAgentLatitude() {
        return agentLatitude;
    }

    public void setAgentLatitude(double agentLatitude) {
        this.agentLatitude = agentLatitude;
    }

    public double getAgentLongitude() {
        return agentLongitude;
    }

    public void setAgentLongitude(double agentLongitude) {
        this.agentLongitude = agentLongitude;
    }

    double agentLatitude;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


    public double getCustomerLatitude() {
        return customerLatitude;
    }

    public void setCustomerLatitude(double customerLatitude) {
        this.customerLatitude = customerLatitude;
    }

    public double getCustomerLongitude() {
        return customerLongitude;
    }

    public void setCustomerLongitude(double customerLongitude) {
        this.customerLongitude = customerLongitude;
    }


    public static Comparator<CoordinateDAO> LatitudeComparator = new Comparator<CoordinateDAO>() {
        @Override
        public int compare(CoordinateDAO coordinateDAO1, CoordinateDAO coordinateDAO2) {
            double distance1 = coordinateDAO1.getDistance();
            double distance2 = coordinateDAO2.getDistance();
            return Double.valueOf(distance1).compareTo(distance2);
        }

    };


}
