
package com.softage.airteldms.dao.tokenDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenDAO {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("expiration")
    @Expose
    private String expiration;
    @SerializedName("role")
    @Expose
    private String role;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
