
package com.softage.airteldms.dao.userDetailsDAO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentDetails {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logEnable")
    @Expose
    private Boolean logEnable;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("circle")
    @Expose
    private String circle;
    @SerializedName("minimumDocuments")
    @Expose
    private Integer minimumDocuments;
    @SerializedName("ftpDetail")
    @Expose
    private FtpDetail ftpDetail;
    @SerializedName("documentTypeDetails")
    @Expose
    private List<DocumentTypeDetail> documentTypeDetails = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getLogEnable() {
        return logEnable;
    }

    public void setLogEnable(Boolean logEnable) {
        this.logEnable = logEnable;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public FtpDetail getFtpDetail() {
        return ftpDetail;
    }

    public void setFtpDetail(FtpDetail ftpDetail) {
        this.ftpDetail = ftpDetail;
    }

    public List<DocumentTypeDetail> getDocumentTypeDetails() {
        return documentTypeDetails;
    }

    public void setDocumentTypeDetails(List<DocumentTypeDetail> documentTypeDetails) {
        this.documentTypeDetails = documentTypeDetails;
    }

    public Integer getImageCount() {
        return minimumDocuments;
    }

    public void setImageCount(Integer imageCount) {
        this.minimumDocuments = imageCount;
    }
}
