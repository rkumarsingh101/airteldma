package com.softage.airteldms.dao.nevDrawerItem;

/**
 * Created by Ravi on 29/07/15.
 */
public class NavDrawerItem {
    private String title;
    private int image;

    public void setImage(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
