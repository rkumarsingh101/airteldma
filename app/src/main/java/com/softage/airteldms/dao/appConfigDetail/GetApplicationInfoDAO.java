
package com.softage.airteldms.dao.appConfigDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetApplicationInfoDAO {

    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("applicationid")
    @Expose
    private String applicationid;
    @SerializedName("apikey")
    @Expose
    private String apikey;
    @SerializedName("databaseurl")
    @Expose
    private String databaseurl;
    @SerializedName("gcmsenderid")
    @Expose
    private String gcmsenderid;
    @SerializedName("projectid")
    @Expose
    private String projectid;
    @SerializedName("storagebucket")
    @Expose
    private String storagebucket;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getApplicationid() {
        return applicationid;
    }

    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getDatabaseurl() {
        return databaseurl;
    }

    public void setDatabaseurl(String databaseurl) {
        this.databaseurl = databaseurl;
    }

    public String getGcmsenderid() {
        return gcmsenderid;
    }

    public void setGcmsenderid(String gcmsenderid) {
        this.gcmsenderid = gcmsenderid;
    }

    public String getProjectid() {
        return projectid;
    }

    public void setProjectid(String projectid) {
        this.projectid = projectid;
    }

    public String getStoragebucket() {
        return storagebucket;
    }

    public void setStoragebucket(String storagebucket) {
        this.storagebucket = storagebucket;
    }

}