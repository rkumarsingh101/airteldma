
package com.softage.airteldms.dao.fileDetailsDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileDetails {

    @SerializedName("ftpDetails")
    @Expose
    private FtpDetails ftpDetails;
    @SerializedName("filePath")
    @Expose
    private String filePath;

    public FtpDetails getFtpDetails() {
        return ftpDetails;
    }

    public void setFtpDetails(FtpDetails ftpDetails) {
        this.ftpDetails = ftpDetails;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
