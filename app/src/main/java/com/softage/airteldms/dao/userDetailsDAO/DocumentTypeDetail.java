
package com.softage.airteldms.dao.userDetailsDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentTypeDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("documentType")
    @Expose
    private String documentType;
    @SerializedName("showType")
    @Expose
    private String showType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }
}
