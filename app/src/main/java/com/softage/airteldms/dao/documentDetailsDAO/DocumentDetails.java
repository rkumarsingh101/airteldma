
package com.softage.airteldms.dao.documentDetailsDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocumentDetails {

    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("documentCount")
    @Expose
    private String imageCount;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("rejectionReasonList")
    @Expose
    private List<String> rejectionReason = null;
    @SerializedName("documentType")
    @Expose
    private String documentType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getImageCount() {
        return imageCount;
    }

    public void setImageCount(String imageCount) {
        this.imageCount = imageCount;
    }

    public List<String> getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(List<String> rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
}
