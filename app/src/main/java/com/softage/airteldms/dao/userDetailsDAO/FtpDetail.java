
package com.softage.airteldms.dao.userDetailsDAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FtpDetail {

    @SerializedName("ftpip")
    @Expose
    private String ftpip;
    @SerializedName("ftpUser")
    @Expose
    private String ftpUser;
    @SerializedName("ftpPassword")
    @Expose
    private String ftpPassword;
    @SerializedName("ftpPort")
    @Expose
    private Integer ftpPort;
    @SerializedName("ftpDirectory")
    @Expose
    private String ftpDirectory;

    public String getFtpip() {
        return ftpip;
    }

    public void setFtpip(String ftpip) {
        this.ftpip = ftpip;
    }

    public String getFtpUser() {
        return ftpUser;
    }

    public void setFtpUser(String ftpUser) {
        this.ftpUser = ftpUser;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public Integer getFtpPort() {
        return ftpPort;
    }

    public void setFtpPort(Integer ftpPort) {
        this.ftpPort = ftpPort;
    }

    public String getFtpDirectory() {
        return ftpDirectory;
    }

    public void setFtpDirectory(String ftpDirectory) {
        this.ftpDirectory = ftpDirectory;
    }

}
