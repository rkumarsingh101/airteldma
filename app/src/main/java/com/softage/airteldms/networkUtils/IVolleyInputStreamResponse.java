package com.softage.airteldms.networkUtils;

/**
 * Created by SS0111 on 2/7/2017.
 */

public interface IVolleyInputStreamResponse {
    void response(byte[] response);
}
