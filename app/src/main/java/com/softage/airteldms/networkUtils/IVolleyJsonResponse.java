package com.softage.airteldms.networkUtils;


import org.json.JSONObject;

public interface IVolleyJsonResponse {
    void jsonResponse(JSONObject response);

}
