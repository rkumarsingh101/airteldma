package com.softage.airteldms.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.softage.airteldms.R;
import com.softage.airteldms.activity.APBCapSearchActivity;
import com.softage.airteldms.activity.APBCapUploadVerification;
import com.softage.airteldms.activity.APBLoginActivity;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.fileDetailsDAO.FileDetailsDAO;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.FilePathManager;
import com.softage.airteldms.utils.LocalPersistenceManager;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class APBHomeFragment extends Fragment {
    private View rootView;
    private LinearLayout lin_layout_cap_upload;
    private LinearLayout lin_layout_cap_search;
    private LinearLayout lin_layout_distributor_download_doc;
    private LinearLayout lin_layout_retailer_download_doc;
    private LinearLayout lin_layout_logout;
    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private String TAG = APBHomeFragment.class.getSimpleName();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.apb_home_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        lin_layout_cap_upload = (LinearLayout) rootView.findViewById(R.id.lin_layout_cap_upload);
        lin_layout_distributor_download_doc = (LinearLayout) rootView.findViewById(R.id.lin_layout_distributor_download_doc);
        lin_layout_retailer_download_doc = (LinearLayout) rootView.findViewById(R.id.lin_layout_retailer_download_doc);
        lin_layout_cap_search = (LinearLayout) rootView.findViewById(R.id.lin_layout_cap_search);
        lin_layout_logout = (LinearLayout) rootView.findViewById(R.id.lin_layout_logout);
        lin_layout_cap_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent myintent = new Intent(getActivity(), APBCapUploadVerification.class);
                startActivity(myintent);


            }
        });
        lin_layout_distributor_download_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    getFileTypeDetail("D");
                } else {
                    GlobalData.showSnackbar(getActivity(), rootView, "No Internet Connection", GlobalData.ColorType.ERROR);
                }


            }
        });
        lin_layout_retailer_download_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    getFileTypeDetail("R");
                } else {
                    GlobalData.showSnackbar(getActivity(), rootView, "No Internet Connection", GlobalData.ColorType.ERROR);
                }


            }
        });
        lin_layout_cap_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent myintent = new Intent(getActivity(), APBCapSearchActivity.class);
                startActivity(myintent);

            }
        });
        lin_layout_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                logoutDialog("Alert", "Are you sure you want to logout?");

            }
        });
    }

    /**
     * For showing logout dialog while user logout form the application
     *
     * @param messge
     */
    private void logoutDialog(String title, String messge) {
        if (!getActivity().isFinishing()) {

            GlobalData.getApp().showAlertDialogWithOkCancel(getActivity(), title, messge, "OK", "Cancel", R.color.red, R.color.green_dark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    if (CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                        LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                        Intent i1 = new Intent(getActivity(), APBLoginActivity.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i1);
                        getActivity().finish();

                    } else {
                        GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.no_internet_connection), GlobalData.ColorType.ERROR);
                    }
                }
            }, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                }
            });
        }
    }

    private void getFileTypeDetail(String fileType) {
        GlobalData.getApp().showProgressDialog(getActivity(), "Fetching Document Detail", "Please wait");
        MyVolleyRequestParsing.getWithHeader(Constants.GetFileDetailsToDownload + "?fileType=" + fileType, LocalPersistenceManager.getLocalPersistenceManager().getHeader(),
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {

                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {
                            try {
                                if (MyVolleyRequestParsing.response_code == 200) {
                                    Gson mGson = new Gson();
                                    FileDetailsDAO userDetailDAO = mGson.fromJson(response, FileDetailsDAO.class);
                                    if (userDetailDAO != null && userDetailDAO.getStatusCode() == 200) {
                                        if (userDetailDAO.getFileDetails() == null || userDetailDAO.getFileDetails().getFilePath().equalsIgnoreCase("") || userDetailDAO.getFileDetails().getFtpDetails().getFtpip().equalsIgnoreCase("") || userDetailDAO.getFileDetails().getFtpDetails().getFtpPassword().equalsIgnoreCase("") || userDetailDAO.getFileDetails().getFtpDetails().getFtpUser().equalsIgnoreCase("") || userDetailDAO.getFileDetails().getFtpDetails().getFtpPort() == null) {
                                            GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                                        } else {
                                            if (fileType.equalsIgnoreCase("D")) {
                                                new DownloadEnrollmentFormDetailFromSftp(userDetailDAO, FilePathManager.DownloadingImageType.DISTRIBUTOR_ENROLLMENT_FORM).execute();
                                            } else {
                                                new DownloadEnrollmentFormDetailFromSftp(userDetailDAO, FilePathManager.DownloadingImageType.RETAILER_ENROLLMENT_FORM).execute();
                                            }
                                        }


                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 201) {
                                        GlobalData.showSnackbar(getActivity(), rootView, userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 202) {

                                        if (!getActivity().isFinishing()) {
                                            GlobalData.getApp().showAlertDialogWithOk(getActivity(), "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    GlobalData.getApp().dismissProgressDialog();

                                                }
                                            });
                                        }

                                        GlobalData.showSnackbar(getActivity(), rootView, userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 205) {
                                        GlobalData.showSnackbar(getActivity(), rootView, userDetailDAO.getStatusMessage(), GlobalData.ColorType.SUCCESS);
                                        if (!getActivity().isFinishing()) {
                                            GlobalData.getApp().showAlertDialogWithOk(getActivity(), "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    GlobalData.getApp().dismissProgressDialog();

                                                }
                                            });
                                        }
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 500) {
                                        GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                    } else {
                                        GlobalData.showSnackbar(getActivity(), rootView, "Please try again", GlobalData.ColorType.ERROR);
                                    }


                                } else if (MyVolleyRequestParsing.response_code == 401) {
                                    GlobalData.showSnackbar(getActivity(), rootView, "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                            Intent myintent = new Intent(getActivity(), APBLoginActivity.class);
                                            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            if (myintent != null) {
                                                startActivity(myintent);
                                                getActivity().finish();
                                            }
                                        }
                                    }, 2000);
                                } else {
                                    GlobalData.showSnackbar(getActivity(), rootView, "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                }


                            } catch (Exception e) {
                                GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.contact_to_support_team), GlobalData.ColorType.ERROR);
                                e.printStackTrace();

                            }


                        }


                    }
                });
    }


    class DownloadEnrollmentFormDetailFromSftp extends AsyncTask<String, Void, Boolean> {
        private FilePathManager.DownloadingImageType downloadingImageType;
        private FileDetailsDAO userDetailDAO;


        public DownloadEnrollmentFormDetailFromSftp(FileDetailsDAO userDetailDAO, FilePathManager.DownloadingImageType downloadingImageType) {
            this.downloadingImageType = downloadingImageType;
            this.userDetailDAO = userDetailDAO;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            GlobalData.getApp().showProgressDialog(getActivity(), "Downloading your document", "Please wait");
        }

        @Override
        protected Boolean doInBackground(String... voids) {

            boolean isAgentPhotoSFTPFileDownloaded = downloadFileFromSftp(userDetailDAO, FilePathManager.getDownloadingImageFilePath(getActivity(), LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getUserId(), this.downloadingImageType.getImageType()));
            if (isAgentPhotoSFTPFileDownloaded) {
                return true;
            } else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            GlobalData.getApp().dismissProgressDialog();
            if (status) {
                try {
                    String downloadedFilePath = FilePathManager.getDownloadingImageFilePath(getActivity(), LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getUserId(), this.downloadingImageType.getImageType());
                    Uri idCardPdfFileURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName(), new File(downloadedFilePath));
                    Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                    intentShareFile.setType("application/zip");
                    intentShareFile.putExtra(Intent.EXTRA_STREAM, idCardPdfFileURI);
                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                            "Enrollment form|" + LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getUserId());
                    intentShareFile.putExtra(Intent.EXTRA_TEXT, "Please find your Enrollment form.");
                    intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(intentShareFile, "Share File"));
                } catch (Exception e) {
                    GlobalData.showSnackbar(getActivity(), rootView, "No any application is installed on your device for sharing your ID card", GlobalData.ColorType.ERROR);
                }


            } else {
                GlobalData.showSnackbar(getActivity(), rootView, "Something went wrong, Please rerty to download sample form.", GlobalData.ColorType.ERROR);
            }
        }

    }

    private boolean downloadFileFromSftp(FileDetailsDAO userDetailDAO, String localFilePath) {
        boolean isFileDownloadedSuccessfully = false;
        String ftpusern = userDetailDAO.getFileDetails().getFtpDetails().getFtpUser();
        String ftpip = userDetailDAO.getFileDetails().getFtpDetails().getFtpip();
        String ftppswd = userDetailDAO.getFileDetails().getFtpDetails().getFtpPassword();
        int SFTPPORT = userDetailDAO.getFileDetails().getFtpDetails().getFtpPort();
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(ftpusern, ftpip, SFTPPORT);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(ftppswd);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.get(userDetailDAO.getFileDetails().getFilePath(), localFilePath);
            sftpChannel.exit();
            session.disconnect();
            isFileDownloadedSuccessfully = true;
        } catch (JSchException e) {
            isFileDownloadedSuccessfully = false;
            e.printStackTrace();
        } catch (SftpException e) {
            isFileDownloadedSuccessfully = false;
            e.printStackTrace();
        }
        return isFileDownloadedSuccessfully;
    }
}
