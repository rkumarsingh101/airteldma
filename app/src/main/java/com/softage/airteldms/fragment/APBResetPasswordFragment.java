package com.softage.airteldms.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.resetPasswordDAO.ResetPasswordDAO;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.LocalPersistenceManager;

import org.json.JSONException;
import org.json.JSONObject;


public class APBResetPasswordFragment extends Fragment {
    private View rootView;
    private String TAG = APBResetPasswordFragment.class.getSimpleName();
    private AppCompatEditText editTextUserId;
    private AppCompatEditText editTextOldPassword;
    private AppCompatEditText editTextNewPassword;
    private AppCompatEditText editTextConfirmPassword;
    private MaterialButton btnCreatePassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.apb_reset_password_fragment, container, false);
        editTextUserId = (AppCompatEditText) rootView.findViewById(R.id.edit_text_user_id);
        editTextOldPassword = (AppCompatEditText) rootView.findViewById(R.id.editText_old_password);
        editTextNewPassword = (AppCompatEditText) rootView.findViewById(R.id.editText_new_password);
        editTextConfirmPassword = (AppCompatEditText) rootView.findViewById(R.id.editText_confirm_password);
        btnCreatePassword = (MaterialButton) rootView.findViewById(R.id.btn_create_password);
        btnCreatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if (isAllFieldValidated()) {
                        sendVerificationData();
                    }
                } else {
                    GlobalData.showSnackbar(getActivity(), getActivity().findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                }
            }
        });
        GlobalData.getApp().hideSoftKeyboard(getActivity());
        return rootView;
    }

    private boolean isAllFieldValidated() {
        boolean isAllFieldValid = true;

        if (editTextUserId.getText().toString().trim().equals("")) {
            GlobalData.showSnackbar(getActivity(), rootView, "Enter user id", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextOldPassword.getText().toString().equals("")) {
            GlobalData.showSnackbar(getActivity(), rootView, "Enter old password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextNewPassword.getText().toString().equals("")) {
            GlobalData.showSnackbar(getActivity(), rootView, "Enter new password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextConfirmPassword.getText().toString().equals("")) {
            GlobalData.showSnackbar(getActivity(), rootView, "Enter confirm password", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextNewPassword.getText().toString().length() <= 7) {
            GlobalData.showSnackbar(getActivity(), rootView, "New password length should be between 8-10 characters", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (editTextConfirmPassword.getText().toString().length() <= 7) {
            GlobalData.showSnackbar(getActivity(), rootView, "Confirm password length should be between 8-10 characters", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        } else if (!editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            GlobalData.showSnackbar(getActivity(), rootView, "New and confirm password does not match", GlobalData.ColorType.ERROR);
            isAllFieldValid = false;
        }

        return isAllFieldValid;
    }

    private void sendVerificationData() {


        JSONObject params = new JSONObject();
        try {
            params.put("userName", editTextUserId.getText().toString().trim());
            params.put("oldPassword", editTextOldPassword.getText().toString());
            params.put("newPassword", editTextNewPassword.getText().toString());
        } catch (JSONException e) {
            return;
        }
        GlobalData.getApp().showProgressDialog(getActivity(), "Initiating", "Please wait");
        MyVolleyRequestParsing.jsonPostWithHeader(Constants.ResetPassword, LocalPersistenceManager.getLocalPersistenceManager().getHeader(), params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            GlobalData.showSnackbar(getActivity(), rootView, getResources().getString(R.string.try_again), GlobalData.ColorType.ERROR);
                        } else {


                            if (MyVolleyRequestParsing.response_code == 200 || MyVolleyRequestParsing.response_code == 201) {
                                Gson mGson = new Gson();
                                ResetPasswordDAO resetPasswordDAO = mGson.fromJson(response, ResetPasswordDAO.class);
                                try {
                                    if (resetPasswordDAO != null && (resetPasswordDAO.getAdditionalStatusCode() == 200)) {
                                        LocalPersistenceManager.getLocalPersistenceManager().setUserPassword(editTextNewPassword.getText().toString());
                                        editTextUserId.setText("");
                                        editTextOldPassword.setText("");
                                        editTextNewPassword.setText("");
                                        editTextConfirmPassword.setText("");
                                        GlobalData.showSnackbar(getActivity(), rootView, resetPasswordDAO.getMessage(), GlobalData.ColorType.SUCCESS);
                                    } else if (resetPasswordDAO != null && !resetPasswordDAO.getMessage().equals("")) {
                                        GlobalData.showSnackbar(getActivity(), rootView, resetPasswordDAO.getMessage().toString(), GlobalData.ColorType.ERROR);
                                    } else {
                                        GlobalData.showSnackbar(getActivity(), rootView, "Please Try Again", GlobalData.ColorType.ERROR);
                                    }


                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    GlobalData.showSnackbar(getActivity(), rootView, "Please Try Again", GlobalData.ColorType.ERROR);
                                }
                            } else {
                                GlobalData.showSnackbar(getActivity(), rootView, "Please Try Again", GlobalData.ColorType.ERROR);
                            }
                        }
                    }
                });
    }


    @Override
    public void onDestroyView() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        super.onDestroyView();
    }
}
