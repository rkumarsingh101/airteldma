package com.softage.airteldms.utils;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;

/**
 * Class for detecting Fake location when agents are using.
 *
 * @author SS0111 Ranjan
 */
public class MockLocationDetector {


    private static final String TAG = MockLocationDetector.class.getSimpleName();


    /**
     * For checking current location is fake or not.
     *
     * @param context
     * @param location
     * @return
     */
    public static boolean isLocationFromMockProvider(Context context, Location location) {
        boolean isMock = false;
        if (Build.VERSION.SDK_INT >= 18) {
            isMock = location.isFromMockProvider();
        } else {
            //isMock = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
            if (Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
                return false;
            else {
                return true;
            }
        }
        //return false;
        return isMock;
    }
}