package com.softage.airteldms.utils;


import com.google.gson.Gson;
import com.softage.airteldms.constants.AppConstants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.userDetailsDAO.AgentDetails;

/**
 * @author SS0111 Ranjan
 * This class is used for saving data in sharedpreference for future use.
 */

public class LocalPersistenceManager {
    private static LocalPersistenceManager localPersistenceManager;


    public static LocalPersistenceManager getLocalPersistenceManager() {
        if (localPersistenceManager == null) {
            localPersistenceManager = new LocalPersistenceManager();
        }


        return localPersistenceManager;

    }


    public void setDeviceID(String deviceID) {
        GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_DEVICE_ID, deviceID).commit();
    }


    public String getDeviceID() {
        return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_DEVICE_ID, "");


    }


    public void setFCMRegistrationID(String registrationID) {
        GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_FCM_REGISTRATION_ID, registrationID).commit();

    }


    public String getFCMRegistrationID() {
        return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_FCM_REGISTRATION_ID, "");


    }

    public void setUserToken(String status) {

        try {
            GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_USER_TOKEN, status).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserToken() {

        try {
            return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_USER_TOKEN, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public void setRunnerLoginStatus(boolean status) {

        try {
            GlobalData.getApp().getSharedPreferences().edit().putBoolean(AppConstants.PREF_KEY_RUNNER_LOGIN_STATUS, status).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean getRunnerLoginStatus() {
        try {
            return GlobalData.getApp().getSharedPreferences().getBoolean(AppConstants.PREF_KEY_RUNNER_LOGIN_STATUS, false);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }




    public void setAgentDetailsDAO(AgentDetails mUser) {
        Gson mGson = new Gson();
        String str_json = mGson.toJson(mUser);
        try {
            GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_AGENT_DETAIL, str_json).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public AgentDetails getAgentDetailsDAO() {
        Gson mGson = new Gson();
        String json = "";
        AgentDetails obj = null;
        try {
            json = GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_AGENT_DETAIL, "");
            obj = mGson.fromJson(json, AgentDetails.class);
        } catch (Exception e) {
            e.printStackTrace();

        }


        return obj;
    }


    public void setHeader(String status) {

        try {
            GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_HEADER, status).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getHeader() {

        try {
            return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_HEADER, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setUserCode(String status) {

        try {
            GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_AGENT_CODE, status).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserCode() {

        try {
            return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_AGENT_CODE, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public void setUserPassword(String status) {

        try {
            GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_AGENT_PASSWORD, status).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserPassword() {

        try {
            return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_AGENT_PASSWORD, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setAgentRole(String deviceID) {
        GlobalData.getApp().getSharedPreferences().edit().putString(AppConstants.PREF_KEY_USER_ROLE, deviceID).commit();
    }


    public String getAgentRole() {
        return GlobalData.getApp().getSharedPreferences().getString(AppConstants.PREF_KEY_USER_ROLE, "");
    }

    public void logoutAllUser() {
        LocalPersistenceManager.getLocalPersistenceManager().setRunnerLoginStatus(false);

    }


}
