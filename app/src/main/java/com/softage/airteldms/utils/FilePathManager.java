package com.softage.airteldms.utils;

/**
 * Class for Managing constant methods, that are used for creating, saving and deleting folder and file in Internal or External storage of device.
 *
 * @author SS0111 Ranjan
 */

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

public class FilePathManager {
    static String captured_folder_name = "CapturedImages";
    static String compressed_folder_name = "CompressedImages";
    static String pdf_folder_name = "CompressedImagesPDF";
    static String downloaded_folder_name = "DownloadedFiles";
    static String DownloadImages_folder_name = "DownloadImages";
    static String app_log_folder_name = "LinkAppLogFile";
    static String zip_file_folder_name = "ZipFile";
    static String mearge_pdf_folder_name = "MeargePDFFile";
    static String signature_folder_name = "SignatureFile";

    public enum DownloadingImageType {
        RETAILER_ENROLLMENT_FORM(1),
        DISTRIBUTOR_ENROLLMENT_FORM(2);

        private final int imageType;

        // private enum constructor
        private DownloadingImageType(int imageType) {
            this.imageType = imageType;
        }

        public int getImageType() {
            return imageType;
        }
    }


    public static void checkFolderImageDeletee(Context context, String cust_id) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), captured_folder_name + "/" + cust_id);
        if (file != null && file.exists()) {
            if (file.listFiles().length < 3) {
                deleteDirectoryFile(file);
            }
        }

    }

    public static File getImageStorageBasePath(Context context, String cust_id) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), captured_folder_name + "/" + cust_id);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file;

    }

    public static File getBasePath(Context context) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), captured_folder_name);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file;

    }

    public static File getCompressedImagessBasePath(Context context) {
        File mFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), compressed_folder_name);
        if (mFile != null && !mFile.exists()) {
            mFile.mkdirs();
        }
        return mFile;
    }

    public static File getPDFBasePath(Context context) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), pdf_folder_name);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file;

    }

    public static File getAppLogFileBasePath(Context context) {
        File mFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), app_log_folder_name);
        if (mFile != null && !mFile.exists()) {
            mFile.mkdirs();
        }
        return mFile;
    }

    public static File getZipFileBasePath(Context context, String zipFileName) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), zip_file_folder_name);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        File taskAllocationLogFile = new File(root_sub_dir, zipFileName + ".zip");
        /*if (!taskAllocationLogFile.exists()) {
            try {
                taskAllocationLogFile.createNewFile();
            } catch (IOException e) {
                taskAllocationLogFile = null;
                e.printStackTrace();
            }
        }*/
        return taskAllocationLogFile;
    }

    public static File getAppLogFile(Context context) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), app_log_folder_name);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        File taskAllocationLogFile = new File(root_sub_dir, "VILCorpLogFile.txt");
        if (!taskAllocationLogFile.exists()) {
            try {
                taskAllocationLogFile.createNewFile();
            } catch (IOException e) {
                taskAllocationLogFile = null;
                e.printStackTrace();
            }
        }
        return taskAllocationLogFile;

    }

    public static File getAppLogFileIfExist(Context context) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), app_log_folder_name);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        File taskAllocationLogFile = new File(root_sub_dir, "VILCorpLogFile.txt");
        return taskAllocationLogFile;

    }


    public static File makeImageStoringFolder(Context context, String cust_id) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), captured_folder_name + "/" + cust_id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return root_sub_dir;

    }

    public static String getCompressedImagessPath(Context context, String cust_id) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), compressed_folder_name + "/" + cust_id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return root_sub_dir.getAbsolutePath();
    }

    public static File makePDFStoringFolder(Context context, String cust_id) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), pdf_folder_name + "/" + cust_id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return root_sub_dir;

    }

    public static File getFilePath(Context context, String folder) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), captured_folder_name + "/" + folder);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file;

    }

    public static File getPDFFilePath(Context context, String folder) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), pdf_folder_name + "/" + folder);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file;

    }


    public static void deleteCapturedImageFiles(Context context) {
        File root_sub_dir_captured_image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), captured_folder_name);

        deleteDirectoryFile(root_sub_dir_captured_image);
    }

    public static void deleteCompressedImageFiles(Context context) {
        File root_sub_dir_compressed_image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), compressed_folder_name);

        deleteDirectoryFile(root_sub_dir_compressed_image);
    }

    public static void deleteCompressedPDFFiles(Context context) {
        File root_sub_dir_compressed_image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), pdf_folder_name);

        deleteDirectoryFile(root_sub_dir_compressed_image);
    }

    public static void deleteMeargePDFFiles(Context context) {
        File root_sub_dir_compressed_image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), mearge_pdf_folder_name);

        deleteDirectoryFile(root_sub_dir_compressed_image);
    }

    public static void deleteSignatureFiles(Context context) {
        File root_sub_dir_compressed_image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), signature_folder_name);

        deleteDirectoryFile(root_sub_dir_compressed_image);
    }

    public static File makeDownloadImageFolder(Context context, String id) {
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), DownloadImages_folder_name + "/" + id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return root_sub_dir;

    }


    public static File getDownloadImageStoringFile(Context context, String id, String imageFileName) {
        String filePath = "";
        filePath = "/" + imageFileName;
        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), DownloadImages_folder_name + "/" + id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return new File(root_sub_dir.getAbsolutePath() + filePath);

    }

    public static String getDownloadingImageFilePath(Context context, String id, int id_card_image_type) {
        String filePath = "";
        if (id_card_image_type == DownloadingImageType.RETAILER_ENROLLMENT_FORM.getImageType()) {
            filePath = "/" + "retailer_enrollment_doc.zip";
        } else if (id_card_image_type == DownloadingImageType.DISTRIBUTOR_ENROLLMENT_FORM.getImageType()) {
            filePath = "/" + "distributor_enrollment_doc.zip";
        }

        File root_dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (root_dir != null && !root_dir.exists()) {
            root_dir.mkdirs();

        }
        File root_sub_dir = new File(root_dir.getAbsolutePath(), DownloadImages_folder_name + "/" + id);
        if (root_sub_dir != null && !root_sub_dir.exists()) {
            root_sub_dir.mkdirs();
        }
        return root_sub_dir.getAbsolutePath() + filePath;

    }


    private static void deleteDirectoryFile(File path) {

        if (path.isDirectory()) {
            String[] children = path.list();
            for (int i = 0; i < children.length; i++) {
                File child = new File(path, children[i]);
                if (child.isDirectory()) {
                    deleteDirectoryFile(child);
                    child.delete();
                } else {
                    child.delete();

                }
            }
        }
    }
}
