package com.softage.airteldms.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.coordinateDAO.CoordinateDAO;
import com.softage.airteldms.dao.distanceDAO.DistanceDAO;
import com.softage.airteldms.dao.latLngAddressDAO.LatLngAddressDAO;
import com.softage.airteldms.dao.reverseGeoCodingDao.ReverseGeoCodingDao;
import com.softage.airteldms.dao.wayPointDAO.WayPointDAO;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class GeoLocationManager {
    private Context mContext;
    GeoLocationManager manager = null;
    private String GOOGLE_REVERSE_GEOCODING_BASE_URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private String GOOGLE_DISTANCE_CALCULATION_BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?&origins=";
    private String GOOGLE_GEOCODE_WAYPOINT_BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    private String TAG = GeoLocationManager.class.getSimpleName();
    private ReverseGeoCodingDao reverseGeoCodingDao;
    private IAddressResolver iAddressResolver;
    private ILatLngResolver iLatLngResolver;
    private IWayPointResolver iWayPointResolver;
    private ILatLngAddressResolver iLatLngAddressResolver;
    private Disposable updatableLocationDisposable;
    private Disposable reverseGeocodeLocationDisposable;
    private String google_api_key;
    public static final int OK = 0;
    public static final int ERROR = 1;
    public static final int FAKE_LOCATION = 2;
    boolean isMokeLocationFound = false;
    private boolean isDistanceCalculationFromGoogleAPI = false;


    public GeoLocationManager(Context mContext) {
        this.mContext = mContext;
    }

    interface IAddressResolver {
        void addressResolved(String formattedAddress, boolean status);

    }

    public interface IWayPointResolver {
        void resolvedWaypoint(WayPointDAO waypointOrder, String message, boolean status);
    }

    public void registerIAddressResolver(IAddressResolver iAddressResolver) {
        this.iAddressResolver = iAddressResolver;
    }

    public void registerIWayPointResolver(IWayPointResolver iWayPointResolver) {
        this.iWayPointResolver = iWayPointResolver;
    }


    public interface ILatLngAddressResolver {
        void latLngAddressResolver(int responseCode, String message, LatLngAddressDAO mLatLngAddressDAO);
    }

    public interface ILatLngResolver {
        void latLngResolved(int responseCode, String message, ArrayList<CoordinateDAO> mCoordinateDAOS);
    }

    public void registerILatLngAddressResolver(ILatLngAddressResolver iLatLngAddressResolver) {
        this.iLatLngAddressResolver = iLatLngAddressResolver;
    }

    public void registerILatLngResolver(ILatLngResolver iLatLngResolver) {
        this.iLatLngResolver = iLatLngResolver;
    }

   /* public void getLatLngToAddressViaGoogleAPI(final Context mContext, final String lat, final String lng, final boolean isProgressDialogRequired) {
        google_api_key = mContext.getResources().getString(R.string.key_first) + mContext.getResources().getString(R.string.key_sec) + mContext.getResources().getString(R.string.key_third) + mContext.getResources().getString(R.string.key_fourth);
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your address is being found", "Please Wait");
        }

        MyVolleyRequestParsing.get(GOOGLE_REVERSE_GEOCODING_BASE_URL + lat + "," + lng + "&key=" + google_api_key + "&language=en",
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response_string) {
                        if (isProgressDialogRequired) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                        clearResource();

                        if (mContext != null) {
                            if (response_string.equals("")) {
                                sendResolvingResult("Address did Not resolved, " + mContext.getResources().getString(R.string.no_internet_connection), false);
                            } else {
                                Gson mGson = new Gson();
                                reverseGeoCodingDao = null;
                                reverseGeoCodingDao = mGson.fromJson(new String(response_string), ReverseGeoCodingDao.class);
                                try {
                                    if (reverseGeoCodingDao != null && reverseGeoCodingDao.getStatus().equalsIgnoreCase("OK")) {
                                        if (reverseGeoCodingDao != null && reverseGeoCodingDao.getResults().size() >= 1) {
                                            for (int i = 0; i < reverseGeoCodingDao.getResults().size(); i++) {
                                                String formattedAddress = reverseGeoCodingDao.getResults().get(i).getFormattedAddress();
                                                sendResolvingResult(formattedAddress, true);
                                                break;

                                            }

                                        }
                                    } else {
                                        sendResolvingResult("Lat and Lng did not resolved", false);
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sendResolvingResult("Lat and Lng did not resolved", false);
                                    if (BuildConfig.LOG)
                                    Log.e("Exception", "Message" + e.getMessage());
                                }


                            }
                        }
                    }
                });

    }*/

    public void getLatLngToAddress(final Context mContext, final double lat, final double lng, final boolean isProgressDialogRequired) {
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your address is being found", "Please Wait");
        }
        reverseGeocodeLocationDisposable = GlobalData.getApp().getReactiveLocationProvider()
                .getReverseGeocodeObservable(Locale.ENGLISH, lng, lng, 4).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Address>>() {
                    @Override
                    public void accept(List<Address> addresses) throws Exception {
                        if (isProgressDialogRequired) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                        boolean isAddressFound = false;
                        int foundAddressLine = 0;

                        for (int i = 0; i < addresses.size(); i++) {
                            switch (i) {
                                case 0:
                                    isAddressFound = true;
                                    foundAddressLine = 0;
                                    break;
                                case 1:
                                    isAddressFound = true;
                                    foundAddressLine = 1;
                                    break;
                                case 2:
                                    isAddressFound = true;
                                    foundAddressLine = 2;
                                    break;
                                case 3:
                                    isAddressFound = true;
                                    foundAddressLine = 3;
                                    break;
                            }
                            if (isAddressFound) {
                                String locationAddress = addresses.get(i).getAddressLine(foundAddressLine);
                                if (BuildConfig.LOG)
                                Log.e(TAG, "reverseGeocodeObservable->>>>>>" + " Address :" + locationAddress);

                                LatLngAddressDAO mLatLngAddressDAO = new LatLngAddressDAO();
                                mLatLngAddressDAO.setAddress(locationAddress);
                                mLatLngAddressDAO.setLatitude(lat);
                                mLatLngAddressDAO.setLongitude(lng);
                                if (iLatLngAddressResolver != null) {
                                    iLatLngAddressResolver.latLngAddressResolver(OK, "Geo location successfully fetched", mLatLngAddressDAO);
                                }
                                clearResource();
                                break;
                            } else {
                                if (iLatLngAddressResolver != null) {
                                    iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                                }
                                clearResource();

                            }
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (isProgressDialogRequired) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                        if (iLatLngAddressResolver != null) {
                            iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                        }
                        clearResource();
                    }
                });
    }

    private void sendResolvingResult(String formattedAddress, boolean status) {
        if (iAddressResolver != null) {
            iAddressResolver.addressResolved(formattedAddress, status);
        }


    }

    double source_lat = 0.0;
    double source_lng = 0.0;
    String source_address = "";
    String destination_address = "";
    double distance = 0.0;

    public void getDistanceFromLatAndLng(final Context mContext, final double dest_lat, final double dest_lng, final boolean isProgressDialogRequired) {
        google_api_key = mContext.getResources().getString(R.string.key_first) + mContext.getResources().getString(R.string.key_sec) + mContext.getResources().getString(R.string.key_third) + mContext.getResources().getString(R.string.key_fourth);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (iLatLngResolver != null) {
                iLatLngResolver.latLngResolved(ERROR, "Location permission is not granted", null);
            }
            clearResource();
            return;
        }
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your distance is being found", "Please Wait");
        }
        isMokeLocationFound = false;
        source_lat = 0.0;
        source_lng = 0.0;
        source_address = "";
        destination_address = "";
        distance = 0.0;
        updatableLocationDisposable = GlobalData.getApp().getReactiveLocationProvider().getUpdatedLocation(GlobalData.getApp().getLocationRequest()).subscribe(new Consumer<Location>() {
            @Override
            public void accept(final Location location) throws Exception {
                if (BuildConfig.LOG)
                Log.e(TAG, "agentLocationUpdate->>>>>>" + " Latitude :" + location.getLatitude() + " getCustomer_longitude :" + location.getLongitude());
                if (MockLocationDetector.isLocationFromMockProvider(mContext, location)) {
                    isMokeLocationFound = true;
                }
                source_lat = location.getLatitude();
                source_lng = location.getLongitude();
                if (mContext != null) {
                    if (isDistanceCalculationFromGoogleAPI) {
                        final ArrayList<CoordinateDAO> mAgentLocationArrayList = new ArrayList<>();

                        String formatted_customer_address = source_lat + "," + source_lng + "&destinations=" + dest_lat + "," + dest_lng;
                        MyVolleyRequestParsing.get(GOOGLE_DISTANCE_CALCULATION_BASE_URL + formatted_customer_address + "&key=" + google_api_key,
                                TAG, new IVolleyResponse() {
                                    @Override
                                    public void response(String response_string) {
                                        if (isProgressDialogRequired) {
                                            GlobalData.getApp().dismissProgressDialog();
                                        }

                                        if (mContext != null) {
                                            if (response_string.equals("")) {
                                                if (iLatLngResolver != null) {
                                                    iLatLngResolver.latLngResolved(ERROR, "Customer latitude or longitude or Address is not valid", null);
                                                }
                                                clearResource();
                                            } else {
                                                Gson mGson = new Gson();
                                                DistanceDAO gDistanceDAo = mGson.fromJson(response_string, DistanceDAO.class);
                                                try {
                                                    if (!gDistanceDAo.getStatus().equalsIgnoreCase("OK")) {
                                                        if (iLatLngResolver != null) {
                                                            iLatLngResolver.latLngResolved(ERROR, "Customer latitude or longitude or Address is not valid", null);
                                                        }
                                                        clearResource();
                                                    } else {
                                                        for (int i = 0; i < gDistanceDAo.getRows().get(0).getElements().size(); i++) {
                                                            if (gDistanceDAo.getRows().get(0).getElements().get(i).getStatus().equalsIgnoreCase("OK")) {
                                                                CoordinateDAO mCoordinateDAO = new CoordinateDAO();
                                                                String splitedDistance[] = gDistanceDAo.getRows().get(0).getElements().get(i).getDistance().getText().split(" ");
                                                                double distance = 0.0;
                                                                if (splitedDistance != null && splitedDistance.length == 2) {

                                                                    if (splitedDistance[1].equalsIgnoreCase("m")) {
                                                                        distance = Double.parseDouble(gDistanceDAo.getRows().get(0).getElements().get(i).getDistance().getText().split(" ")[0].replace(",", ""));
                                                                        distance = distance / 1000;
                                                                    } else {
                                                                        distance = Double.parseDouble(gDistanceDAo.getRows().get(0).getElements().get(i).getDistance().getText().split(" ")[0].replace(",", ""));
                                                                    }
                                                                }
                                                                mCoordinateDAO.setDistance(distance);
                                                                mCoordinateDAO.setCustomerLatitude(dest_lat);
                                                                mCoordinateDAO.setCustomerLongitude(dest_lng);
                                                                mCoordinateDAO.setAgentLatitude(location.getLatitude());
                                                                mCoordinateDAO.setAgentLongitude(location.getLongitude());
                                                                mCoordinateDAO.setAgentAddress(gDistanceDAo.getOriginAddresses().get(0));
                                                                mCoordinateDAO.setCustomerAddress(gDistanceDAo.getDestinationAddresses().get(0));
                                                                mAgentLocationArrayList.add(mCoordinateDAO);
                                                                Collections.sort(mAgentLocationArrayList, CoordinateDAO.LatitudeComparator);
                                                                if (iLatLngResolver != null) {
                                                                    iLatLngResolver.latLngResolved(isMokeLocationFound ? FAKE_LOCATION : OK, "Distance is fetched successfully", mAgentLocationArrayList);
                                                                }
                                                                clearResource();
                                                                break;

                                                            } else {
                                                                if (iLatLngResolver != null) {
                                                                    iLatLngResolver.latLngResolved(ERROR, "Customer latitude or longitude or Address is not valid", null);
                                                                }
                                                                clearResource();
                                                                break;
                                                            }

                                                        }
                                                        if (gDistanceDAo.getRows().get(0).getElements().size() == 0) {
                                                            if (iLatLngResolver != null) {
                                                                iLatLngResolver.latLngResolved(ERROR, "Customer latitude or longitude or Address is not valid", null);
                                                            }
                                                            clearResource();
                                                        }


                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    if (iLatLngResolver != null) {
                                                        iLatLngResolver.latLngResolved(ERROR, "Customer latitude or longitude or Address is not valid", null);
                                                    }
                                                    clearResource();
                                                    if (BuildConfig.LOG)
                                                    Log.e("Exception", e.getMessage());
                                                }
                                            }

                                        }
                                    }
                                });
                    } else {
                        final ArrayList<CoordinateDAO> mAgentLocationArrayList = new ArrayList<>();
                        reverseGeocodeLocationDisposable = GlobalData.getApp().getReactiveLocationProvider()
                                .getReverseGeocodeObservable(Locale.ENGLISH, source_lat, source_lng, 4).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Address>>() {
                                    @Override
                                    public void accept(List<Address> addresses) throws Exception {
                                        boolean isAddressFound = false;
                                        int foundAddressLine = 0;

                                        for (int i = 0; i < addresses.size(); i++) {
                                            if (isAddressFound) {
                                                break;
                                            }
                                            switch (i) {
                                                case 0:
                                                    isAddressFound = true;
                                                    foundAddressLine = 0;
                                                    break;
                                                case 1:
                                                    isAddressFound = true;
                                                    foundAddressLine = 1;
                                                    break;
                                                case 2:
                                                    isAddressFound = true;
                                                    foundAddressLine = 2;
                                                    break;
                                                case 3:
                                                    isAddressFound = true;
                                                    foundAddressLine = 3;
                                                    break;
                                            }
                                        }

                                        if (updatableLocationDisposable != null && !updatableLocationDisposable.isDisposed()) {
                                            updatableLocationDisposable.dispose();
                                        }
                                        if (reverseGeocodeLocationDisposable != null && !reverseGeocodeLocationDisposable.isDisposed()) {
                                            reverseGeocodeLocationDisposable.dispose();
                                        }

                                        if (isAddressFound) {
                                            source_address = addresses.get(foundAddressLine).getAddressLine(foundAddressLine);
                                            if (BuildConfig.LOG)
                                            Log.e(TAG, "reverseGeocodeObservable->>>>>>" + "Source Address :" + source_address);
                                            Location destinationLocation = new Location("DestinationLocation");
                                            destinationLocation.setLatitude(dest_lat);
                                            destinationLocation.setLongitude(dest_lng);
                                            distance = Double.parseDouble(String.format("%.2f", (location.distanceTo(destinationLocation) / 1000)));
                                            reverseGeocodeLocationDisposable = GlobalData.getApp().getReactiveLocationProvider()
                                                    .getReverseGeocodeObservable(Locale.ENGLISH, dest_lat, dest_lng, 4).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Address>>() {
                                                        @Override
                                                        public void accept(List<Address> addresses) throws Exception {
                                                            boolean isAddressFound = false;
                                                            int foundAddressLine = 0;

                                                            for (int i = 0; i < addresses.size(); i++) {
                                                                if (isAddressFound) {
                                                                    break;
                                                                }
                                                                switch (i) {
                                                                    case 0:
                                                                        isAddressFound = true;
                                                                        foundAddressLine = 0;
                                                                        break;
                                                                    case 1:
                                                                        isAddressFound = true;
                                                                        foundAddressLine = 1;
                                                                        break;
                                                                    case 2:
                                                                        isAddressFound = true;
                                                                        foundAddressLine = 2;
                                                                        break;
                                                                    case 3:
                                                                        isAddressFound = true;
                                                                        foundAddressLine = 3;
                                                                        break;
                                                                }
                                                            }
                                                            if (updatableLocationDisposable != null && !updatableLocationDisposable.isDisposed()) {
                                                                updatableLocationDisposable.dispose();
                                                            }
                                                            if (reverseGeocodeLocationDisposable != null && !reverseGeocodeLocationDisposable.isDisposed()) {
                                                                reverseGeocodeLocationDisposable.dispose();
                                                            }

                                                            if (isAddressFound) {
                                                                destination_address = addresses.get(foundAddressLine).getAddressLine(foundAddressLine);
                                                                Log.e(TAG, "reverseGeocodeObservable->>>>>>" + "Destination Address :" + destination_address);
                                                                CoordinateDAO mCoordinateDAO = new CoordinateDAO();
                                                                mCoordinateDAO.setDistance(distance);
                                                                mCoordinateDAO.setCustomerLatitude(dest_lat);
                                                                mCoordinateDAO.setCustomerLongitude(dest_lng);
                                                                mCoordinateDAO.setAgentLatitude(source_lat);
                                                                mCoordinateDAO.setAgentLongitude(source_lng);
                                                                mCoordinateDAO.setAgentAddress(source_address);
                                                                mCoordinateDAO.setCustomerAddress(destination_address);
                                                                mAgentLocationArrayList.add(mCoordinateDAO);
                                                                Collections.sort(mAgentLocationArrayList, CoordinateDAO.LatitudeComparator);
                                                                if (isProgressDialogRequired) {
                                                                    GlobalData.getApp().dismissProgressDialog();
                                                                }
                                                                if (iLatLngResolver != null) {
                                                                    iLatLngResolver.latLngResolved(isMokeLocationFound ? FAKE_LOCATION : OK, "Success", mAgentLocationArrayList);
                                                                }
                                                                clearResource();

                                                            } else {
                                                                Toast.makeText(mContext, "Address not found for customer location, please try again", Toast.LENGTH_LONG).show();
                                                                if (isProgressDialogRequired) {
                                                                    GlobalData.getApp().dismissProgressDialog();
                                                                }
                                                                if (iLatLngResolver != null) {
                                                                    iLatLngResolver.latLngResolved(ERROR, "Address not found for customer location, please try again", null);
                                                                }
                                                                clearResource();

                                                            }

                                                        }
                                                    }, new Consumer<Throwable>() {
                                                        @Override
                                                        public void accept(Throwable throwable) throws Exception {
                                                            Toast.makeText(mContext, "Address not found for customer location, please try again", Toast.LENGTH_LONG).show();
                                                            if (isProgressDialogRequired) {
                                                                GlobalData.getApp().dismissProgressDialog();
                                                            }
                                                            if (iLatLngResolver != null) {
                                                                iLatLngResolver.latLngResolved(ERROR, "Address not found for customer location, please try again", null);
                                                            }
                                                            clearResource();
                                                        }
                                                    });


                                        } else {
                                            Toast.makeText(mContext, "Address not found for your current location, please try again", Toast.LENGTH_LONG).show();
                                            if (isProgressDialogRequired) {
                                                GlobalData.getApp().dismissProgressDialog();
                                            }
                                            if (iLatLngResolver != null) {
                                                iLatLngResolver.latLngResolved(ERROR, "Address not found for your current location, please try again", null);
                                            }
                                            clearResource();

                                        }

                                    }
                                }, new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Toast.makeText(mContext, "Address not found for your current location, please try again", Toast.LENGTH_LONG).show();
                                        if (isProgressDialogRequired) {
                                            GlobalData.getApp().dismissProgressDialog();
                                        }
                                        if (iLatLngResolver != null) {
                                            iLatLngResolver.latLngResolved(ERROR, "Address not found for your current location, please try again", null);
                                        }
                                        clearResource();
                                    }
                                });

                    }


                } else {
                    if (isProgressDialogRequired) {
                        GlobalData.getApp().dismissProgressDialog();
                    }
                    if (iLatLngResolver != null) {
                        iLatLngResolver.latLngResolved(ERROR, "Your current location or address is not fetched", null);
                    }
                    clearResource();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (isProgressDialogRequired) {
                    GlobalData.getApp().dismissProgressDialog();
                }
                if (iLatLngResolver != null) {
                    iLatLngResolver.latLngResolved(ERROR, "Error in fetching distance", null);
                }
                clearResource();
            }
        });


    }

    public void getGeoCodeOptimizeWayPoint(final Context mContext, final String source_lat, final String source_lng, final String dest_lat, final String dest_lng, final String wayPoints, final boolean isProgressDialogRequired) {
        google_api_key = mContext.getResources().getString(R.string.key_first) + mContext.getResources().getString(R.string.key_sec) + mContext.getResources().getString(R.string.key_third) + mContext.getResources().getString(R.string.key_fourth);
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your traveling way point is being calculated", "Please Wait");
        }
        String formatted_way_points = source_lat + "," + source_lng + "&destination=" + dest_lat + "," + dest_lng + "&waypoints=optimize:true|" + wayPoints;
        MyVolleyRequestParsing.get(GOOGLE_GEOCODE_WAYPOINT_BASE_URL + formatted_way_points + "&key=" + google_api_key,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response_string) {
                        if (isProgressDialogRequired) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                        try {
                            if (response_string.equals("")) {
                                if (iWayPointResolver != null) {
                                    iWayPointResolver.resolvedWaypoint(null, "Your traveling way point is not found, " + mContext.getResources().getString(R.string.try_again), false);
                                }
                            } else {
                                Gson mGson = new Gson();
                                WayPointDAO mGeocodedWaypointsDAO = mGson.fromJson(new String(response_string), WayPointDAO.class);
                                if (mGeocodedWaypointsDAO != null && mGeocodedWaypointsDAO.getStatus().equalsIgnoreCase("OK")) {
                                    if (mGeocodedWaypointsDAO.getRoutes() != null && mGeocodedWaypointsDAO.getRoutes().get(0) != null) {
                                        if (iWayPointResolver != null) {
                                            iWayPointResolver.resolvedWaypoint(mGeocodedWaypointsDAO, "Your traveling way point is found successfully", true);
                                        }
                                    } else {
                                        if (iWayPointResolver != null) {
                                            iWayPointResolver.resolvedWaypoint(null, "Error in fetching way points, " + mContext.getResources().getString(R.string.contact_to_support_team), false);
                                        }
                                    }
                                } else {
                                    if (iWayPointResolver != null) {
                                        iWayPointResolver.resolvedWaypoint(null, "Error in fetching way points, " + mContext.getResources().getString(R.string.contact_to_support_team), false);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (iWayPointResolver != null) {
                                iWayPointResolver.resolvedWaypoint(null, "Error in fetching way points, " + mContext.getResources().getString(R.string.try_again), false);
                            }
                        }

                    }
                });
    }


    public void getCurrentLatLngAddress(final boolean isProgressDialogRequired) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (iLatLngAddressResolver != null) {
                iLatLngAddressResolver.latLngAddressResolver(ERROR, "Location permission is not granted", null);
            }
            clearResource();
            return;
        }
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your Geo Location is being fetched", "Please Wait");
        }
        isMokeLocationFound = false;
        updatableLocationDisposable = GlobalData.getApp().getReactiveLocationProvider().getUpdatedLocation(GlobalData.getApp().getLocationRequest()).subscribe(new Consumer<Location>() {
            @Override
            public void accept(Location location) throws Exception {
                Log.e(TAG, "agentLocationUpdate->>>>>>" + " Latitude :" + location.getLatitude() + " getCustomer_longitude :" + location.getLongitude());
                if (MockLocationDetector.isLocationFromMockProvider(mContext, location)) {
                    isMokeLocationFound = true;
                }
                final double latitude = location.getLatitude();
                final double longitude = location.getLongitude();
                reverseGeocodeLocationDisposable = GlobalData.getApp().getReactiveLocationProvider()
                        .getReverseGeocodeObservable(Locale.ENGLISH, location.getLatitude(), location.getLongitude(), 4).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Address>>() {
                            @Override
                            public void accept(List<Address> addresses) throws Exception {
                                if (isProgressDialogRequired) {
                                    GlobalData.getApp().dismissProgressDialog();
                                }
                                boolean isAddressFound = false;
                                int foundAddressLine = 0;

                                for (int i = 0; i < addresses.size(); i++) {
                                    switch (i) {
                                        case 0:
                                            isAddressFound = true;
                                            foundAddressLine = 0;
                                            break;
                                        case 1:
                                            isAddressFound = true;
                                            foundAddressLine = 1;
                                            break;
                                        case 2:
                                            isAddressFound = true;
                                            foundAddressLine = 2;
                                            break;
                                        case 3:
                                            isAddressFound = true;
                                            foundAddressLine = 3;
                                            break;
                                    }
                                    if (isAddressFound) {
                                        String locationAddress = addresses.get(i).getAddressLine(foundAddressLine);
                                        Log.e(TAG, "reverseGeocodeObservable->>>>>>" + " Address :" + locationAddress);
                                        LatLngAddressDAO mLatLngAddressDAO = new LatLngAddressDAO();
                                        mLatLngAddressDAO.setAddress(locationAddress);
                                        mLatLngAddressDAO.setLatitude(latitude);
                                        mLatLngAddressDAO.setLongitude(longitude);
                                        if (iLatLngAddressResolver != null) {
                                            iLatLngAddressResolver.latLngAddressResolver(isMokeLocationFound ? FAKE_LOCATION : OK, "Geo location successfully fetched", mLatLngAddressDAO);
                                        }
                                        clearResource();
                                        break;
                                    } else {
                                        if (iLatLngAddressResolver != null) {
                                            iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                                        }
                                        clearResource();

                                    }
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (isProgressDialogRequired) {
                                    GlobalData.getApp().dismissProgressDialog();
                                }
                                if (iLatLngAddressResolver != null) {
                                    iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                                }
                                clearResource();
                            }
                        });


            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (isProgressDialogRequired) {
                    GlobalData.getApp().dismissProgressDialog();
                }
                if (iLatLngAddressResolver != null) {
                    iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                }
                clearResource();
            }
        });


    }

    public void getCurrentLatLngAddressOptional(final boolean isProgressDialogRequired) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (iLatLngAddressResolver != null) {
                iLatLngAddressResolver.latLngAddressResolver(ERROR, "Location permission is not granted", null);
            }
            clearResource();
            return;
        }
        if (isProgressDialogRequired) {
            GlobalData.getApp().showProgressDialog(mContext, "Your Geo Location is being fetched", "Please Wait");
        }
        isMokeLocationFound = false;
        updatableLocationDisposable = GlobalData.getApp().getReactiveLocationProvider().getUpdatedLocation(GlobalData.getApp().getLocationRequest()).subscribe(new Consumer<Location>() {
            @Override
            public void accept(Location location) throws Exception {
                Log.e(TAG, "agentLocationUpdate->>>>>>" + " Latitude :" + location.getLatitude() + " getCustomer_longitude :" + location.getLongitude());
                if (MockLocationDetector.isLocationFromMockProvider(mContext, location)) {
                    isMokeLocationFound = true;
                }
                final double latitude = location.getLatitude();
                final double longitude = location.getLongitude();
                reverseGeocodeLocationDisposable = GlobalData.getApp().getReactiveLocationProvider()
                        .getReverseGeocodeObservable(Locale.ENGLISH, location.getLatitude(), location.getLongitude(), 4).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Address>>() {
                            @Override
                            public void accept(List<Address> addresses) throws Exception {
                                if (isProgressDialogRequired) {
                                    GlobalData.getApp().dismissProgressDialog();
                                }
                                boolean isAddressFound = false;
                                int foundAddressLine = 0;

                                for (int i = 0; i < addresses.size(); i++) {
                                    switch (i) {
                                        case 0:
                                            isAddressFound = true;
                                            foundAddressLine = 0;
                                            break;
                                        case 1:
                                            isAddressFound = true;
                                            foundAddressLine = 1;
                                            break;
                                        case 2:
                                            isAddressFound = true;
                                            foundAddressLine = 2;
                                            break;
                                        case 3:
                                            isAddressFound = true;
                                            foundAddressLine = 3;
                                            break;
                                    }
                                    if (isAddressFound) {
                                        String locationAddress = addresses.get(i).getAddressLine(foundAddressLine);
                                        Log.e(TAG, "reverseGeocodeObservable->>>>>>" + " Address :" + locationAddress);
                                        LatLngAddressDAO mLatLngAddressDAO = new LatLngAddressDAO();
                                        mLatLngAddressDAO.setAddress(locationAddress);
                                        mLatLngAddressDAO.setLatitude(latitude);
                                        mLatLngAddressDAO.setLongitude(longitude);
                                        if (iLatLngAddressResolver != null) {
                                            iLatLngAddressResolver.latLngAddressResolver(isMokeLocationFound ? FAKE_LOCATION : OK, "Geo location successfully fetched", mLatLngAddressDAO);
                                        }
                                        clearResource();
                                        break;
                                    } else {
                                        LatLngAddressDAO mLatLngAddressDAO = new LatLngAddressDAO();
                                        mLatLngAddressDAO.setAddress("Address was not able to fetched");
                                        mLatLngAddressDAO.setLatitude(latitude);
                                        mLatLngAddressDAO.setLongitude(longitude);
                                        if (iLatLngAddressResolver != null) {
                                            iLatLngAddressResolver.latLngAddressResolver(isMokeLocationFound ? FAKE_LOCATION : OK, "Geo location successfully fetched", mLatLngAddressDAO);
                                        }

                                        clearResource();

                                    }
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (isProgressDialogRequired) {
                                    GlobalData.getApp().dismissProgressDialog();
                                }
                                LatLngAddressDAO mLatLngAddressDAO = new LatLngAddressDAO();
                                mLatLngAddressDAO.setAddress("Address was not able to fetched");
                                mLatLngAddressDAO.setLatitude(latitude);
                                mLatLngAddressDAO.setLongitude(longitude);
                                if (iLatLngAddressResolver != null) {
                                    iLatLngAddressResolver.latLngAddressResolver(isMokeLocationFound ? FAKE_LOCATION : OK, "Geo location successfully fetched", mLatLngAddressDAO);
                                }
                                clearResource();
                            }
                        });


            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (isProgressDialogRequired) {
                    GlobalData.getApp().dismissProgressDialog();
                }
                clearResource();
                if (iLatLngAddressResolver != null) {
                    iLatLngAddressResolver.latLngAddressResolver(ERROR, "Geo location not fetched", null);
                }
                clearResource();
            }
        });


    }

    private void clearResource() {
        if (updatableLocationDisposable != null && !updatableLocationDisposable.isDisposed()) {
            updatableLocationDisposable.dispose();
        }
        if (reverseGeocodeLocationDisposable != null && !reverseGeocodeLocationDisposable.isDisposed()) {
            reverseGeocodeLocationDisposable.dispose();
        }
        iAddressResolver = null;
        iLatLngResolver = null;
        iLatLngAddressResolver = null;
    }
}