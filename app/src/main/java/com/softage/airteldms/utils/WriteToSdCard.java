package com.softage.airteldms.utils;

import android.Manifest;
import android.content.Context;
import android.util.Log;

import com.softage.airteldms.constants.GlobalData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import pub.devrel.easypermissions.EasyPermissions;

public class WriteToSdCard {
    private Context mContext;
    final static String[] perms = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    public WriteToSdCard(Context mContext) {
        this.mContext = mContext;
    }


    public void writeTextToSdCard(String message_type, String message) {

        if (!checkPermission(mContext)) {
            Log.e("checkPermission", "Read and write permission not granted yet");
            return;
        }
        if (LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO() != null && LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getLogEnable() != null && LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getLogEnable()) {
            File myFile = FilePathManager.getAppLogFile(mContext);
            try {
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append("\n" + getCurrentDateAndTime() + " :" + message_type + " :" + message + "\n" + "**********************************************************************************************************" + "\n");
                myOutWriter.close();
                fOut.close();

            } catch (Exception e) {

            }
        } else {
            File myFile = FilePathManager.getAppLogFileIfExist(mContext);
            if (myFile != null && myFile.exists()) {
                GlobalData.getApp().deleteLoggerFile(myFile.getAbsolutePath());
            }
        }

    }

    private String getCurrentDateAndTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private boolean checkPermission(Context mContext) {
        if (EasyPermissions.hasPermissions(mContext, perms)) {
            return true;
        } else {
            return false;
        }
    }


}
