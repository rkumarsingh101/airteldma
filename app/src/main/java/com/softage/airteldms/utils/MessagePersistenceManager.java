package com.softage.airteldms.utils;

import android.content.Context;

import com.softage.airteldms.R;


public class MessagePersistenceManager {
    private static MessagePersistenceManager messagePersistenceManager;


    public static MessagePersistenceManager getMessagePersistenceManager() {
        if (messagePersistenceManager == null) {
            messagePersistenceManager = new MessagePersistenceManager();
        }
        return messagePersistenceManager;

    }

    public String noInternetConnectionMessage(Context mContextCompat) {
        return mContextCompat.getResources().getString(R.string.no_internet_connection);
    }

    public String contactToSupportTeamMessage(Context mContextCompat) {
        return mContextCompat.getResources().getString(R.string.contact_to_support_team);
    }
}
