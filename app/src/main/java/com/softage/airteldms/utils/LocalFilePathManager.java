package com.softage.airteldms.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class LocalFilePathManager {
    static String signature_folder_name = "SignatureFile";

    public static File getRespondentDigitalSignatureFile(Context context) {
        String filePath = "";
        filePath = "/" + "respondent_signature.png";
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), signature_folder_name);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return new File(file.getAbsolutePath() + filePath);

    }

    public static String getRespondentDigitalSignaturePath(Context context) {
        String filePath = "";

        filePath = "/" + "respondent_signature.png";

        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), signature_folder_name);
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath() + filePath;

    }
}
