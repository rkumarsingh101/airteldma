package com.softage.airteldms.imageCapture;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.softage.airteldms.R;
import com.softage.airteldms.adaptors.AllCapturedImagePreviewAdaptor;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.utils.LocalPersistenceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AllCapturedImagePreviewFragment extends DialogFragment implements AllCapturedImagePreviewAdaptor.OnCapturedImageDeleteClicked {
    private View rootView;
    private OnAddListener onAddListener;
    private OnOkListener onOkListener;
    private OnDeleteListener onDeleteListener;
    private LinearLayout lin_layout_add;
    private LinearLayout lin_layout_ok;
    private RecyclerView recycler_view_image_preview;
    private AllCapturedImagePreviewAdaptor allCapturedImagePreviewAdaptor;
    public static final String EXTRA_TAKEN_IMAGE_PATH = "EXTRA_TAKEN_IMAGE_PATH";
    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private String image_directory_path;
    private List<File> fileList;
    private AppCompatTextView txt_view_data_not_available;
    private int minimumNoOfImageCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setCancelable(false);
        getDialog().setTitle("Captured Image Preview");
        View view = inflater.inflate(R.layout.all_captured_image_preview_dialog_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = view;
        lin_layout_add = (LinearLayout) rootView.findViewById(R.id.lin_layout_add);
        lin_layout_ok = (LinearLayout) rootView.findViewById(R.id.lin_layout_ok);
        recycler_view_image_preview = (RecyclerView) rootView.findViewById(R.id.recycler_view_image_preview);
        txt_view_data_not_available = (AppCompatTextView) rootView.findViewById(R.id.txt_view_data_not_available);
        txt_view_data_not_available.setVisibility(View.GONE);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_view_image_preview.setLayoutManager(mLayoutManager);
        recycler_view_image_preview.setItemAnimator(new DefaultItemAnimator());
        recycler_view_image_preview.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
        Bundle mVBundle = getArguments();
        if (mVBundle != null && mVBundle.containsKey(EXTRA_TAKEN_IMAGE_PATH) && mVBundle.containsKey("minimumNoOfImageCount")) {
            image_directory_path = mVBundle.getString(EXTRA_TAKEN_IMAGE_PATH);
            File directory = new File(image_directory_path);
            File[] files = directory.listFiles();
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.compare(f1.lastModified(), f2.lastModified());
                }
            });

            fileList = new ArrayList<>(Arrays.asList(files));
            minimumNoOfImageCount = mVBundle.getInt("minimumNoOfImageCount");
            if (allCapturedImagePreviewAdaptor == null) {
                allCapturedImagePreviewAdaptor = new AllCapturedImagePreviewAdaptor(getActivity(), fileList);
                allCapturedImagePreviewAdaptor.setOnCapturedImageDeleteClicked(AllCapturedImagePreviewFragment.this);
                recycler_view_image_preview.setAdapter(allCapturedImagePreviewAdaptor);
            } else {
                allCapturedImagePreviewAdaptor.notifyDataSetChanged();
            }

           /* mFileCapturedImage = new File(image_path);
            if (mFileCapturedImage != null && mFileCapturedImage.exists()) {
                Bitmap capturedSelfie = BitmapFactory.decodeFile(image_path);
                image_view_captured_image.setImageBitmap(capturedSelfie);
            } else {
                Toast.makeText(getActivity(), "Something wrong, please retake image.", Toast.LENGTH_LONG).show();
            }*/


        }
        lin_layout_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!getActivity().isFinishing()) {
                    GlobalData.getApp().showAlertDialogWithOkCancel(getActivity(), "Info", "Do you want to proceed?", "OK", "Cancel", R.color.green_dark, R.color.colorPrimaryDark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GlobalData.getApp().dismissProgressDialog();
                            if (onOkListener != null) {
                                onOkListener.onOk();
                            }
                            dismiss();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                    });
                }


            }
        });

        lin_layout_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!getActivity().isFinishing()) {
                    GlobalData.getApp().showAlertDialogWithOkCancel(getActivity(), "Info", "Do you want to capture more images?", "OK", "Cancel", R.color.green_dark, R.color.colorPrimaryDark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GlobalData.getApp().dismissProgressDialog();
                            if (onAddListener != null) {
                                onAddListener.onAdd();
                            }
                            dismiss();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GlobalData.getApp().dismissProgressDialog();
                        }
                    });
                }


            }
        });
    }

    public Bitmap flip(Bitmap src) {
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
          /*  dialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {

                        if (onOkListener != null) {
                            onOkListener.onOk();
                        }
                        dismiss();
                        return true; // Capture onKey
                    }
                    return false; // Don't capture
                }
            });*/
        }
    }

    @Override
    public void onCapturedImageDeleteClicked(int position) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (!getActivity().isFinishing()) {
            GlobalData.getApp().showAlertDialogWithOkCancel(getActivity(), "Info", "Do you want to delete this image?", "OK", "Cancel", R.color.colorPrimaryDark, R.color.green_dark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    if (fileList.get(position) != null && fileList.get(position).exists()) {
                        fileList.get(position).delete();
                        fileList.remove(position);
                        if (allCapturedImagePreviewAdaptor != null) {
                            allCapturedImagePreviewAdaptor.notifyDataSetChanged();
                        }
                        if (fileList != null && fileList.size() == 0) {
                            txt_view_data_not_available.setVisibility(View.VISIBLE);
                        }
                        if (fileList.size() < LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getImageCount()) {
                            lin_layout_ok.setVisibility(View.GONE);

                        }
                        if (onDeleteListener != null) {
                            onDeleteListener.onDelete();
                        }

                        CameraXCapturedImagePreviewDialogFragment.image_taken_number--;
                        Toast.makeText(getActivity(), "Image deleted", Toast.LENGTH_LONG).show();

                    }


                }
            }, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                }
            });
        }
    }

    /**
     * Callback that will be called when the dialog is closed due to a confirm button click.
     */
    public interface OnOkListener {
        void onOk();
    }

    public interface OnDeleteListener {
        void onDelete();
    }


    /**
     * Callback that will be called when the dialog is closed due a discard button click.
     */
    public interface OnAddListener {
        void onAdd();
    }

    public void setOnOkListener(@Nullable OnOkListener onOkListener) {
        this.onOkListener = onOkListener;
    }

    public void setOnDeleteListener(@Nullable OnDeleteListener onDeleteListener) {
        this.onDeleteListener = onDeleteListener;
    }


    /**
     * Sets the callback that will be called when the dialog is closed due a discard button click.
     *
     * @param onAddListener
     */
    public void setOnAddListener(@Nullable OnAddListener onAddListener) {
        this.onAddListener = onAddListener;
    }
}
