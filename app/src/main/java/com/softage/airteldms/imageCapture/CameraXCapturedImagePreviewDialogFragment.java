package com.softage.airteldms.imageCapture;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;


import com.softage.airteldms.R;
import com.softage.airteldms.adaptors.AllCapturedImagePreviewAdaptor;
import com.softage.airteldms.utils.ExifUtil;
import com.squareup.picasso.Picasso;

import java.io.File;

public class CameraXCapturedImagePreviewDialogFragment extends DialogFragment {
    private View rootView;
    private AppCompatImageView image_view_captured_image;
    private LinearLayout lin_layout_delete, lin_layout_ok;
    private String image_path;
    public static int image_taken_number;
    private OnConfirmListener onConfirmListener;
    private OnDiscardListener onDiscardListener;
    public static final String EXTRA_TAKEN_IMAGE_PATH = "EXTRA_TAKEN_IMAGE_PATH";
    private File mFileCapturedImage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setCancelable(false);
        getDialog().setTitle("Captured Image");
        View view = inflater.inflate(R.layout.camerax_captured_image_preview_dialog_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = view;
        image_view_captured_image = (AppCompatImageView) rootView.findViewById(R.id.image_view_captured_image);
        lin_layout_ok = (LinearLayout) rootView.findViewById(R.id.lin_layout_ok);
        lin_layout_delete = (LinearLayout) rootView.findViewById(R.id.lin_layout_delete);
        Bundle mVBundle = getArguments();
        if (mVBundle != null && mVBundle.containsKey(EXTRA_TAKEN_IMAGE_PATH)) {
            image_path = mVBundle.getString(EXTRA_TAKEN_IMAGE_PATH);

            mFileCapturedImage = new File(image_path);
            if (mFileCapturedImage != null && mFileCapturedImage.exists()) {
                Bitmap capturedSelfie = BitmapFactory.decodeFile(image_path);
                //Bitmap orientedBitmap = ExifUtil.rotateBitmap(image_path, capturedSelfie);
                // image_view_captured_image.setImageBitmap(capturedSelfie);
                Picasso.get()
                        .load(mFileCapturedImage)
                        .into(image_view_captured_image);
            } else {
                Toast.makeText(getActivity(), "Something wrong, please retake image.", Toast.LENGTH_LONG).show();
            }


        }
        lin_layout_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onConfirmListener != null) {
                    onConfirmListener.onConfirm();
                }
                dismiss();

            }
        });

        lin_layout_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image_path != null && !image_path.equals("")) {
                    if (mFileCapturedImage != null && mFileCapturedImage.exists()) {
                        mFileCapturedImage.delete();
                        Toast.makeText(getActivity(), "Image deleted", Toast.LENGTH_LONG).show();
                        image_taken_number--;
                    }
                }
                if (onDiscardListener != null) {
                    onDiscardListener.onDiscard();
                }
                dismiss();


            }
        });
    }


    public Bitmap flip(Bitmap src) {
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        if (image_path != null && !image_path.equals("")) {
                            if (mFileCapturedImage != null && mFileCapturedImage.exists()) {
                                mFileCapturedImage.delete();
                                Toast.makeText(getActivity(), "Image deleted", Toast.LENGTH_LONG).show();
                                image_taken_number--;
                            }
                        }
                        if (onDiscardListener != null) {
                            onDiscardListener.onDiscard();
                        }
                        dismiss();
                        return true; // Capture onKey
                    }
                    return false; // Don't capture
                }
            });
        }
    }

    /**
     * Callback that will be called when the dialog is closed due to a confirm button click.
     */
    public interface OnConfirmListener {
        void onConfirm();
    }

    /**
     * Callback that will be called when the dialog is closed due a discard button click.
     */
    public interface OnDiscardListener {
        void onDiscard();
    }

    public void setOnConfirmListener(@Nullable OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

    /**
     * Sets the callback that will be called when the dialog is closed due a discard button click.
     *
     * @param onDiscardListener
     */
    public void setOnDiscardListener(@Nullable OnDiscardListener onDiscardListener) {
        this.onDiscardListener = onDiscardListener;
    }
}

