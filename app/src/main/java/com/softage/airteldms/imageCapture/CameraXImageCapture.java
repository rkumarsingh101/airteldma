package com.softage.airteldms.imageCapture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraControl;
import androidx.camera.core.CameraInfoUnavailableException;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.CameraX;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.MeteringPoint;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.softage.airteldms.BuildConfig;
import com.softage.airteldms.R;
import com.softage.airteldms.activity.APBLoginActivity;
import com.softage.airteldms.activity.APBRunnerMainActivity;
import com.softage.airteldms.constants.Constants;
import com.softage.airteldms.constants.GlobalData;
import com.softage.airteldms.dao.statusDAO.StatusDAO;
import com.softage.airteldms.dao.userDetailsDAO.DocumentTypeDetail;
import com.softage.airteldms.networkUtils.CheckNetworkConnection;
import com.softage.airteldms.networkUtils.IVolleyResponse;
import com.softage.airteldms.networkUtils.MyVolleyRequestParsing;
import com.softage.airteldms.utils.FilePathManager;
import com.softage.airteldms.utils.LocalPersistenceManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnCompressListener;
import me.shaohui.advancedluban.OnMultiCompressListener;

public class CameraXImageCapture extends AppCompatActivity implements CameraXCapturedImagePreviewDialogFragment.OnConfirmListener, CameraXCapturedImagePreviewDialogFragment.OnDiscardListener, AllCapturedImagePreviewFragment.OnAddListener, AllCapturedImagePreviewFragment.OnOkListener, AllCapturedImagePreviewFragment.OnDeleteListener {
    private Executor executor = Executors.newSingleThreadExecutor();
    private PreviewView mPreviewView;
    private FloatingActionButton take_picture;
    private long mLastClickTime = 0;
    private long mClickWaitTime = 3000;
    private int rotationDegrees = 0;
    private String current_captured_file_path;
    private ImageCapture imageCapture;
    private CameraXCapturedImagePreviewDialogFragment cameraXCapturedImagePreviewDialogFragment;
    private int no_of_image_to_be_captured = -1;
    public static int CAMERAX_IMAGE_CAPTURED_REQUEST_CODE = 1026;
    private int i_final;
    private ArrayList<Uri> image_file_compressed;
    private String TAG = CameraXImageCapture.class.getSimpleName();
    private File capturedImageFile;
    boolean needToLogout = false;
    private List<String> ftpRemotePathList;
    private String mobileNo = "";
    private DocumentTypeDetail selectedDocTypeDetail = null;
    private int imageCapturedCount = 0;
    private AppCompatTextView txt_view_remaining_images;
    private Camera camera;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camerax_image_capture_activity);
        current_captured_file_path = "";
        image_file_compressed = new ArrayList<Uri>();
        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
        take_picture = (FloatingActionButton) findViewById(R.id.take_picture);
        txt_view_remaining_images = (AppCompatTextView) findViewById(R.id.txt_view_remaining_images);
        mPreviewView = (PreviewView) findViewById(R.id.previewView);
        mPreviewView.setScaleType(PreviewView.ScaleType.FIT_CENTER);

        Bundle value = getIntent().getExtras();
        if (value != null && value.containsKey("mobileNo") && value.containsKey("SelectedDocTypeDetail")) {
            no_of_image_to_be_captured = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getImageCount();
            mobileNo = this.getIntent().getStringExtra("mobileNo");
            Gson mGson = new Gson();
            String strSelectedDocTypeDetail = this.getIntent().getStringExtra("SelectedDocTypeDetail");
            selectedDocTypeDetail = mGson.fromJson(strSelectedDocTypeDetail, DocumentTypeDetail.class);
        }
        if (no_of_image_to_be_captured < 1) {
            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Images to be captured must be more than 0, Please try again.", GlobalData.ColorType.ERROR);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.putExtra("message", "Images to be captured must be more than 0");
                    setResult(RESULT_CANCELED, intent);
                    finish();
                    return;
                }
            }, 2000);
        }


        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < mClickWaitTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (CameraXCapturedImagePreviewDialogFragment.image_taken_number != no_of_image_to_be_captured) {
                    GlobalData.getApp().showProgressDialog(CameraXImageCapture.this, "Capturing image", "Please wait");
                    CameraXCapturedImagePreviewDialogFragment.image_taken_number++;
                    imageCapturedCount = imageCapturedCount + 1;
                    capturedImageFile = new File(FilePathManager.makeImageStoringFolder(CameraXImageCapture.this, mobileNo), mobileNo + "_" + imageCapturedCount + ".png");
                    current_captured_file_path = capturedImageFile.getAbsolutePath();

                    ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(capturedImageFile).build();

                    imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
                        @Override
                        public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    GlobalData.getApp().dismissProgressDialog();
                                    if (capturedImageFile != null && capturedImageFile.exists()) {
                                        i_final = 0;
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        image_file_compressed.removeAll(image_file_compressed);
                                        Bundle args = new Bundle();
                                        args.putString(CameraXCapturedImagePreviewDialogFragment.EXTRA_TAKEN_IMAGE_PATH, current_captured_file_path);
                                        args.putInt("rotationDegree", rotationDegrees);
                                        FragmentManager manager = getSupportFragmentManager();
                                        cameraXCapturedImagePreviewDialogFragment = new CameraXCapturedImagePreviewDialogFragment();
                                        cameraXCapturedImagePreviewDialogFragment.setArguments(args);
                                        cameraXCapturedImagePreviewDialogFragment.setOnConfirmListener(CameraXImageCapture.this);
                                        cameraXCapturedImagePreviewDialogFragment.setOnDiscardListener(CameraXImageCapture.this);
                                        cameraXCapturedImagePreviewDialogFragment.show(manager, "Captured Image");
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("onPictureTaken", "Successfully");
                                    } else {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("onPictureTaken", "File is not created");
                                    }

                                }
                            });
                        }

                        @Override
                        public void onError(@NonNull ImageCaptureException error) {
                            error.printStackTrace();
                            GlobalData.getApp().dismissProgressDialog();
                            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("onCameraError", "Something went wrong, while capturing image" + error.getMessage());
                            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("onCameraError", "Something went wrong, while capturing image" + error.getImageCaptureError());
                            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("onCameraError", "Something went wrong, while capturing image" + error.getLocalizedMessage());
                            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Something went wrong, Please try again.", GlobalData.ColorType.ERROR);
                            Intent intent = new Intent();
                            intent.putExtra("message", "Something went wrong, Please try again");
                            setResult(RESULT_CANCELED, intent);
                            finish();
                        }
                    });
                }
            }
        });


        startCamera();
        //When user first open the camera, all captured and compressed images are deleted(if any) from app internal storage
        FilePathManager.deleteCompressedImageFiles(this);
        FilePathManager.deleteCapturedImageFiles(this);
        showRemainingCount();


    }


    class SaveImageAsyncTask extends AsyncTask<Void, Boolean, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean isFileSavedSuccessFully = true;
            try {
                Bitmap capturedSelfie = null;
                try {
                    capturedSelfie = BitmapFactory.decodeFile(current_captured_file_path);
                    FileOutputStream out = new FileOutputStream(capturedImageFile);
                    capturedSelfie.compress(Bitmap.CompressFormat.PNG, 100, out);
                    try {
                        out.flush();
                        out.close();
                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);

                    } catch (IOException e) {
                        e.printStackTrace();
                        isFileSavedSuccessFully = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isFileSavedSuccessFully = false;
                }


            } catch (Exception e) {
                e.printStackTrace();
                isFileSavedSuccessFully = false;
            }
            return isFileSavedSuccessFully;
        }

        @Override
        protected void onPostExecute(Boolean saveStatus) {
            super.onPostExecute(saveStatus);
            GlobalData.getApp().dismissProgressDialog();
            if (saveStatus) {

                checkTotalImageTaken();
            } else {
                if (current_captured_file_path != null && !current_captured_file_path.equals("")) {
                    File mFileCapturedImage = new File(current_captured_file_path);
                    if (mFileCapturedImage != null && mFileCapturedImage.exists()) {
                        mFileCapturedImage.delete();
                        CameraXCapturedImagePreviewDialogFragment.image_taken_number--;
                    }
                }
                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Captured image is not saved in storage, please try again", GlobalData.ColorType.ERROR);
            }
            showRemainingCount();
        }
    }

    public Bitmap flip(Bitmap src) {
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public String getBatchDirectoryName() {

        String app_folder_path = "";
        app_folder_path = Environment.getExternalStorageDirectory().toString() + "/images";
        File dir = new File(app_folder_path);
        if (!dir.exists() && !dir.mkdirs()) {

        }

        return app_folder_path;
    }

    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {

                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider);

                } catch (ExecutionException | InterruptedException e) {
                    Toast.makeText(CameraXImageCapture.this, "Error in while Starting camera", Toast.LENGTH_LONG).show();
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .build();
        imageAnalysis.setAnalyzer(executor, new ImageAnalysis.Analyzer() {
            @Override
            public void analyze(@NonNull ImageProxy image) {
                rotationDegrees = image.getImageInfo().getRotationDegrees();
                if (BuildConfig.LOG)
                    Log.e("rotationDegrees", "rotationDegrees :" + rotationDegrees);

            }
        });

        imageCapture = new ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build();

        preview.setSurfaceProvider(mPreviewView.createSurfaceProvider());


        camera = cameraProvider.bindToLifecycle((LifecycleOwner) this, cameraSelector, preview, imageAnalysis, imageCapture);
        CameraControl cameraControl = camera.getCameraControl();
        ScaleGestureDetector.SimpleOnScaleGestureListener simpleOnScaleGestureListener =
                new ScaleGestureDetector.SimpleOnScaleGestureListener() {
                    @Override
                    public boolean onScale(ScaleGestureDetector detector) {
                        float currentZoomRatio = camera.getCameraInfo()
                                .getZoomState().getValue().getZoomRatio();

                        float delta = detector.getScaleFactor();

                        cameraControl.setZoomRatio(currentZoomRatio * delta);

                        return true;
                    }
                };
        ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(
                this, simpleOnScaleGestureListener);
        mPreviewView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                scaleGestureDetector.onTouchEvent(motionEvent);
                if (motionEvent.getAction() != MotionEvent.ACTION_UP) {
                    return true;
                }
                Log.e("onTouch", "onTouch");
                MeteringPointFactory meteringPointFactory =
                        mPreviewView.getMeteringPointFactory();
                MeteringPoint meteringPoint = meteringPointFactory.createPoint(
                        motionEvent.getX(), motionEvent.getY());
                FocusMeteringAction action =
                        new FocusMeteringAction.Builder(meteringPoint).build();
                cameraControl.startFocusAndMetering(action);
                return false;
            }
        });

    }

    @Override
    public void onConfirm() {
        if (BuildConfig.LOG)
            Log.e("onConfirm", "onConfirm is called");
        GlobalData.getApp().showProgressDialog(CameraXImageCapture.this, "Saving image to storage", "please wait");
        Luban.compress(capturedImageFile, FilePathManager.getCompressedImagessBasePath(CameraXImageCapture.this))
                // .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnCompressListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(File file) {
                        current_captured_file_path = file.getAbsolutePath();
                        new SaveImageAsyncTask().execute();

                    }

                    @Override
                    public void onError(Throwable e) {
                        GlobalData.getApp().dismissProgressDialog();
                    }


                });

    }

    @Override
    public void onDiscard() {
        if (BuildConfig.LOG)
            Log.e("onDiscard", "onDiscard is called");
        showRemainingCount();
        checkTotalImageTaken();
    }

    @Override
    public void onBackPressed() {
        if (!isFinishing()) {
            GlobalData.getApp().showAlertDialogWithOkCancel(this, "Alert", "Do you want to go back without uploading  image.", "OK", "Cancel", R.color.red, R.color.green_dark, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Your image is not uploaded.", GlobalData.ColorType.ERROR);
                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.putExtra("message", "No any images are taken.");
                            setResult(RESULT_CANCELED, intent);
                            finish();
                        }
                    }, 2000);
                }
            }, new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    GlobalData.getApp().dismissProgressDialog();
                }
            });
        }


    }

    @Override
    protected void onDestroy() {
        GlobalData.getApp().cancelPendingRequests(TAG);
        GlobalData.getApp().dismissProgressDialog();
        super.onDestroy();
    }

    private void checkTotalImageTaken() {
        if (BuildConfig.LOG)
            Log.e("image_taken_number", "" + CameraXCapturedImagePreviewDialogFragment.image_taken_number);
        if (CameraXCapturedImagePreviewDialogFragment.image_taken_number == no_of_image_to_be_captured) {
            if (!isFinishing()) {
                Bundle args = new Bundle();
                File image_directory_file_path = FilePathManager.getImageStorageBasePath(CameraXImageCapture.this, mobileNo);
                if (image_directory_file_path != null && image_directory_file_path.exists() && image_directory_file_path.listFiles().length > 0) {
                    args.putString(CameraXCapturedImagePreviewDialogFragment.EXTRA_TAKEN_IMAGE_PATH, image_directory_file_path.getAbsolutePath());
                    args.putInt("minimumNoOfImageCount", no_of_image_to_be_captured);
                    FragmentManager manager = getSupportFragmentManager();
                    AllCapturedImagePreviewFragment allCapturedImagePreviewFragment = new AllCapturedImagePreviewFragment();
                    allCapturedImagePreviewFragment.setArguments(args);
                    allCapturedImagePreviewFragment.setOnAddListener(CameraXImageCapture.this);
                    allCapturedImagePreviewFragment.setOnOkListener(CameraXImageCapture.this);
                    allCapturedImagePreviewFragment.setOnDeleteListener(CameraXImageCapture.this);
                    allCapturedImagePreviewFragment.show(manager, "Captured Image Preview");
                } else {
                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Something went wrong, " + getResources().getString(R.string.please_try_again), GlobalData.ColorType.ERROR);
                }
            }


        }
    }

    @Override
    public void onOk() {

        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("checkTotalImageTaken", "All images are taken successfully");
        GlobalData.getApp().showProgressDialog(CameraXImageCapture.this, "Compressing Image", "please wait");
        ArrayList<Uri> image_uris = new ArrayList<Uri>();

        File file = FilePathManager.getImageStorageBasePath(CameraXImageCapture.this, mobileNo);
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            Uri image_path_uri = Uri.parse(files[i].getAbsolutePath());
            image_uris.add(image_path_uri);
            if (!new File(image_path_uri.toString()).exists()) {
                GlobalData.getApp().dismissProgressDialog();
                FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Something was wrong while saving image in the storage.", GlobalData.ColorType.ERROR);
                GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("checkTotalImageTaken", "All taken images are not found in storage");
                return;
            }
        }
        final List<File> imagesUriLocal = new ArrayList<>();
        for (int i = 0; i < image_uris.size(); i++) {
            File mFile = new File(image_uris.get(i).getPath());
            imagesUriLocal.add(mFile);
        }
        for (int i = 0; i < imagesUriLocal.size(); i++) {
            image_file_compressed.add(Uri.fromFile(imagesUriLocal.get(i)));
            i_final++;
        }
        if (no_of_image_to_be_captured == i_final) {
            for (int i = 0; i < image_file_compressed.size(); i++) {
                if (!new File(image_file_compressed.get(i).getPath().toString()).exists()) {
                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Something was wrong while saving image in the storage.", GlobalData.ColorType.ERROR);
                    GlobalData.getApp().dismissProgressDialog();
                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "All taken images are not found in storage");
                    return;
                }
                renameFileName(new File(image_file_compressed.get(i).getPath().toString()), imagesUriLocal.get(i));
            }
            image_file_compressed.removeAll(image_file_compressed);


            for (int i = 0; i < imagesUriLocal.size(); i++) {
                image_file_compressed.add(Uri.fromFile(imagesUriLocal.get(i)));

            }
            GlobalData.getApp().dismissProgressDialog();
            if (CheckNetworkConnection.isNetworkAvailable(CameraXImageCapture.this)) {

                File zipFilePath = FilePathManager.getZipFileBasePath(CameraXImageCapture.this, mobileNo);

                if (zip(imagesUriLocal, zipFilePath.getAbsolutePath())) {
                    ArrayList<File> fileArrayList = new ArrayList<>();
                    fileArrayList.add(zipFilePath);
                    new UploadFileToSFTP(fileArrayList).execute();
                } else {
                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("File Zip", "Error in zipping image, please try again");
                    GlobalData.getApp().dismissProgressDialog();
                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in zipping image, please try again", GlobalData.ColorType.SUCCESS);


                }

            } else {
                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
            }

        } else {
            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "Differences in compressed total images and captured total images");
            GlobalData.getApp().dismissProgressDialog();
            FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
            FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
            CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in compressing image, please try again", GlobalData.ColorType.SUCCESS);

        }



       /* Luban.compress(imagesUriLocal, FilePathManager.getCompressedImagessBasePath(CameraXImageCapture.this))
                .putGear(Luban.THIRD_GEAR)
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {
                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "Started");
                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        for (int i = 0; i < fileList.size(); i++) {
                            image_file_compressed.add(Uri.fromFile(fileList.get(i)));
                            i_final++;
                        }
                        if (no_of_image_to_be_captured == i_final) {
                            for (int i = 0; i < image_file_compressed.size(); i++) {
                                if (!new File(image_file_compressed.get(i).getPath().toString()).exists()) {
                                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Something was wrong while saving image in the storage.", GlobalData.ColorType.ERROR);
                                    GlobalData.getApp().dismissProgressDialog();
                                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "All taken images are not found in storage");
                                    return;
                                }
                                renameFileName(new File(image_file_compressed.get(i).getPath().toString()), imagesUriLocal.get(i));
                            }
                            image_file_compressed.removeAll(image_file_compressed);


                            for (int i = 0; i < imagesUriLocal.size(); i++) {
                                image_file_compressed.add(Uri.fromFile(imagesUriLocal.get(i)));

                            }
                            GlobalData.getApp().dismissProgressDialog();
                            if (CheckNetworkConnection.isNetworkAvailable(CameraXImageCapture.this)) {

                                File zipFilePath = FilePathManager.getZipFileBasePath(CameraXImageCapture.this, mobileNo);

                                if (zip(imagesUriLocal, zipFilePath.getAbsolutePath())) {
                                    ArrayList<File> fileArrayList = new ArrayList<>();
                                    fileArrayList.add(zipFilePath);
                                    new UploadFileToSFTP(fileArrayList).execute();
                                } else {
                                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("File Zip", "Error in zipping image, please try again");
                                    GlobalData.getApp().dismissProgressDialog();
                                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in zipping image, please try again", GlobalData.ColorType.SUCCESS);


                                }

                            } else {
                                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "No Internet Connection", GlobalData.ColorType.ERROR);
                            }

                        } else {
                            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "Differences in compressed total images and captured total images");
                            GlobalData.getApp().dismissProgressDialog();
                            FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                            FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                            CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in compressing image, please try again", GlobalData.ColorType.SUCCESS);

                        }


                    }

                    @Override
                    public void onError(final Throwable e) {
                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("Luban.compress", "Error in compression");
                        GlobalData.getApp().dismissProgressDialog();
                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in compressing image, please try again", GlobalData.ColorType.SUCCESS);


                    }
                });*/
        showRemainingCount();
    }

    private static final int BUFFER = 80000;

    public boolean zip(List<File> imagesUriLocal, String zipFileName) {
        boolean isFileZipped = true;
        String _files[] = new String[imagesUriLocal.size()];
        for (int i = 0; i < imagesUriLocal.size(); i++) {
            _files[i] = imagesUriLocal.get(i).getAbsolutePath();
        }
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            isFileZipped = false;
            e.printStackTrace();
        }
        return isFileZipped;
    }

    @Override
    public void onAdd() {

        if (LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getImageCount() <= CameraXCapturedImagePreviewDialogFragment.image_taken_number) {
            no_of_image_to_be_captured = no_of_image_to_be_captured + 1;
        }
        showRemainingCount();
    }

    @Override
    public void onDelete() {
        if (no_of_image_to_be_captured > LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getImageCount()) {
            no_of_image_to_be_captured = no_of_image_to_be_captured - 1;
        }
        showRemainingCount();
    }

    private boolean renameFileName(File from, File to) {
        return from.getParentFile().exists() && from.exists() && from.renameTo(to);
    }


    class UploadFileToSFTP extends AsyncTask<String, String, Boolean> {
        boolean status = false;
        List<File> imagesUriLocal;

        public UploadFileToSFTP(List<File> imagesUriLocal) {
            this.imagesUriLocal = imagesUriLocal;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (ftpRemotePathList == null) {
                ftpRemotePathList = new ArrayList<>();
            }
            ftpRemotePathList.removeAll(ftpRemotePathList);
            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("UploadFileToSFTP", "Start file uploading");
            GlobalData.getApp().showProgressDialog(CameraXImageCapture.this, "Images are uploading to server", "Please wait");
        }


        @Override
        protected Boolean doInBackground(String... strings) {
            for (int i = 0; i < imagesUriLocal.size(); i++) {
                String splited_image_uri[] = imagesUriLocal.get(i).getAbsolutePath().split("/");
                String fileName = splited_image_uri[splited_image_uri.length - 1];
                String remotePath = uploadImageToFTP(imagesUriLocal.get(i).getAbsolutePath(), fileName);
                if (remotePath.equalsIgnoreCase("")) {
                    ftpRemotePathList = null;
                    status = false;
                    break;
                } else {
                    ftpRemotePathList.add(remotePath);
                    status = true;
                }
            }


            return status;

        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);

            if (status) {
                GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("UploadFileToSFTP onPostExecute", "Image is successfully uploaded on the server");
                if (BuildConfig.LOG)
                    Log.e("onPostExecute", "PDF Uploaded");
                for (int i = 0; i < imagesUriLocal.size(); i++) {
                    String splited_image_uri[] = imagesUriLocal.get(i).getAbsolutePath().split("/");
                    String fileName = splited_image_uri[splited_image_uri.length - 1];
                    deleteafterUploading(fileName, mobileNo);
                }

                GlobalData.getApp().dismissProgressDialog();
                if (CheckNetworkConnection.isNetworkAvailable(CameraXImageCapture.this)) {
                    sendAcknowledgementToServer();

                } else {
                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection) + " , uploading fail", GlobalData.ColorType.SUCCESS);
                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                }


            } else {
                GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("UploadFileToSFTP onPostExecute", "Error in Uploading image to server");
                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Error in Uploading image, Please try again.", GlobalData.ColorType.ERROR);
                GlobalData.getApp().dismissProgressDialog();
                FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
            }
            showRemainingCount();

        }


    }


    private String uploadImageToFTP(final String path, final String fileName) {
        java.util.Properties config = null;
        Session session = null;
        JSch jsch = null;
        String SFTPWORKINGDIR = "AirtelPaymentBank/CAF";
        String splited_working_dir[] = SFTPWORKINGDIR.split("/");

        String remotePath = "";
        String ftpusern = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getFtpDetail().getFtpUser();
        String ftpip = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getFtpDetail().getFtpip();
        String ftppswd = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getFtpDetail().getFtpPassword();
        int SFTPPORT = LocalPersistenceManager.getLocalPersistenceManager().getAgentDetailsDAO().getFtpDetail().getFtpPort();

        Channel channel = null;
        ChannelSftp channelSftp = null;
        if (BuildConfig.LOG)
            Log.e(TAG, "preparing the host information for sftp.");

        try {
            if (jsch == null) {
                jsch = new JSch();
            }
            if (session == null) {
                session = jsch.getSession(ftpusern, ftpip, SFTPPORT);
                // session.setTimeout(1000000);
                session.setPassword(ftppswd);
            }
            if (config == null) {
                config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
                session.connect();

            }
            if (BuildConfig.LOG) {
                Log.e("Simex: UploadFile", session.toString());
                Log.e(TAG, "Host connected.");
            }
            if (channel == null) {
                channel = session.openChannel("sftp");
                channel.connect();
            }

            if (BuildConfig.LOG)
                Log.e(TAG, "sftp channel opened and connected.");
            if (channelSftp == null) {
                channelSftp = (ChannelSftp) channel;
            }
            for (int i = 0; i < splited_working_dir.length; i++) {
                try {
                    channelSftp.cd(splited_working_dir[i]);
                } catch (SftpException e) {
                    channelSftp.mkdir(splited_working_dir[i]);
                    channelSftp.cd(splited_working_dir[i]);

                }
            }
            String dir = "";

            dir = SFTPWORKINGDIR + "/" + mobileNo;
            try {
                channelSftp.cd(mobileNo);
            } catch (SftpException e) {
                channelSftp.mkdir(mobileNo);
                channelSftp.cd(mobileNo);

            }
            dir = dir + "/" + selectedDocTypeDetail.getId();
            try {
                channelSftp.cd("" + selectedDocTypeDetail.getId());
            } catch (SftpException e) {
                channelSftp.mkdir("" + selectedDocTypeDetail.getId());
                channelSftp.cd("" + selectedDocTypeDetail.getId());

            }


            File f = new File(path);
            channelSftp.put(new FileInputStream(f), fileName);
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.exit();
                if (BuildConfig.LOG)
                    Log.e(TAG, "sftp Channel exited.");
                channel.disconnect();
                if (BuildConfig.LOG)
                    Log.e(TAG, "Channel disconnected.");
            }
            if (BuildConfig.LOG)
                Log.e(TAG, "File transfered successfully to host.");
            remotePath = dir + "/" + fileName;
            if (BuildConfig.LOG)
                Log.e(TAG, "remotePath :" + remotePath);


        } catch (Exception e) {
            remotePath = "";
            Log.e(TAG, "Exception found while Uploading PDF to SFTP Server." + e.getMessage());
            GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("uploadImageToFTP", "Exception in updating FTP Image" + e.getMessage());
        } finally {
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.exit();
                Log.e(TAG, "sftp Channel exited.");
                channel.disconnect();
                Log.e(TAG, "Channel disconnected.");
            }
        }
        return remotePath;
    }

    //10-08-2021 Delete all images files from internal storage after uploading images to server
    public void deleteafterUploading(String filename, String FolderName) {
        File rootDir = null;
        rootDir = FilePathManager.getPDFBasePath(CameraXImageCapture.this);


        if (rootDir != null) {
            File[] list = rootDir.listFiles();
            for (int i = 0; i < list.length; i++) {
                String name = list[i].getName();

                if (name.equalsIgnoreCase(FolderName)) {
                    if (list[i] != null) {
                        if (list[i].isDirectory()) {
                            for (File c : list[i].listFiles()) {
                                if (c.isFile()) {
                                    if (c.getName().equalsIgnoreCase(filename)) {
                                        c.delete();
                                    }
                                }
                            }


                        }
                    }
                }
            }

        }
        File file1 = null;
        file1 = FilePathManager.getPDFFilePath(CameraXImageCapture.this, FolderName);
        if (file1 != null && file1.exists()) {
            if (file1.listFiles().length == 0) {
                file1.delete();

            }

        }
        File capturedFilePath = FilePathManager.getImageStorageBasePath(CameraXImageCapture.this, mobileNo);
        deleteFolderContent(capturedFilePath);
        deleteFolderContent(FilePathManager.getZipFileBasePath(CameraXImageCapture.this, mobileNo));


    }

    public static boolean deleteFolderContent(File file) {
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i = 0; i < children.length; i++) {
                deleteFolderContent(new File(file, children[i]));

            }
        }
        return file.delete();
    }


    private void sendAcknowledgementToServer() {
        needToLogout = false;
        GlobalData.getApp().showProgressDialog(CameraXImageCapture.this, "Acknowledging to server", "Please wait");
        JSONObject params = new JSONObject();
        try {
            params.put("documentType", selectedDocTypeDetail.getId());
            params.put("documentPaths", TextUtils.join(",", ftpRemotePathList));
            params.put("mobileNo", mobileNo);

        } catch (JSONException e) {
            e.printStackTrace();
            GlobalData.getApp().dismissProgressDialog();
            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Your image is not uploaded successfully, please try again", GlobalData.ColorType.ERROR);
            FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
            FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
            CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
            return;
        }
        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Send FTP path detail to server");
        MyVolleyRequestParsing.jsonPostWithHeader(Constants.UploadDocuments, LocalPersistenceManager.getLocalPersistenceManager().getHeader(), params,
                TAG, new IVolleyResponse() {
                    @Override
                    public void response(String response) {
                        GlobalData.getApp().dismissProgressDialog();
                        if (response.equals("")) {
                            FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                            FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                            CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                            GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Your image is not uploaded successfully, please try again", GlobalData.ColorType.ERROR);
                        } else {
                            try {
                                if (MyVolleyRequestParsing.response_code == 200) {
                                    Gson mGson = new Gson();
                                    StatusDAO userDetailDAO = mGson.fromJson(response, StatusDAO.class);
                                    if (userDetailDAO != null && userDetailDAO.getStatusCode() == 200) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "image is uploaded successfully");
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Successfully uploaded", GlobalData.ColorType.SUCCESS);
                                        if (!isFinishing()) {
                                            GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Info", "Successfully uploaded", "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    GlobalData.getApp().dismissProgressDialog();
                                                    Intent myintent = new Intent(CameraXImageCapture.this, APBRunnerMainActivity.class);
                                                    myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    if (myintent != null) {
                                                        startActivity(myintent);
                                                        finish();
                                                    }
                                                }
                                            });
                                        }

                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 201) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "image is not uploaded " + userDetailDAO.getStatusMessage());
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();

                                            }
                                        });


                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 202) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Selfie is not uploaded " + userDetailDAO.getStatusMessage());
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();


                                            }
                                        });
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 203) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Selfie is not uploaded " + userDetailDAO.getStatusMessage());
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();

                                            }
                                        });
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 204) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Selfie is not uploaded " + userDetailDAO.getStatusMessage());
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.ERROR);
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();


                                            }
                                        });
                                    } else if (userDetailDAO != null && userDetailDAO.getStatusCode() == 500) {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Selfie is not uploaded " + userDetailDAO.getStatusMessage());
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), userDetailDAO.getStatusMessage(), GlobalData.ColorType.SUCCESS);
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        if (!isFinishing()) {
                                            GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", userDetailDAO.getStatusMessage(), "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    GlobalData.getApp().dismissProgressDialog();


                                                }
                                            });
                                        }
                                    } else {
                                        GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Selfie is not uploaded ");
                                        FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                        FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                        CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                        GlobalData.getApp().showAlertDialogWithOk(CameraXImageCapture.this, "Alert", "Something went wrong, please try again", "Ok", R.color.colorPrimary, R.color.white, SweetAlertDialog.NORMAL_TYPE, new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                GlobalData.getApp().dismissProgressDialog();

                                            }
                                        });
                                        GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Your image is not uploaded successfully, Please try again", GlobalData.ColorType.ERROR);
                                    }
                                } else if (MyVolleyRequestParsing.response_code == 401) {
                                    needToLogout = true;
                                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Authentication failed, Please login again");
                                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                            Intent myintent = new Intent(CameraXImageCapture.this, APBLoginActivity.class);
                                            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            if (myintent != null) {
                                                startActivity(myintent);
                                                finish();
                                            }
                                        }
                                    }, 2000);
                                } else {
                                    needToLogout = true;
                                    GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Authentication failed, Please login again");
                                    GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Authentication failed, Please login again", GlobalData.ColorType.ERROR);
                                    FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                    FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                    CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            LocalPersistenceManager.getLocalPersistenceManager().logoutAllUser();
                                            Intent myintent = new Intent(CameraXImageCapture.this, APBLoginActivity.class);
                                            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            if (myintent != null) {
                                                startActivity(myintent);
                                                finish();
                                            }
                                        }
                                    }, 2000);
                                }


                            } catch (Exception e) {
                                GlobalData.getApp().getWriteToSdCardInstance().writeTextToSdCard("sendAcknowledgementToServer", "Exception in updating FTP Path to server " + e.getMessage());
                                FilePathManager.deleteCapturedImageFiles(CameraXImageCapture.this);
                                FilePathManager.deleteCompressedImageFiles(CameraXImageCapture.this);
                                CameraXCapturedImagePreviewDialogFragment.image_taken_number = 0;
                                GlobalData.showSnackbar(CameraXImageCapture.this, findViewById(android.R.id.content), "Your image is not uploaded successfully, please try again", GlobalData.ColorType.ERROR);
                                e.printStackTrace();

                            }


                        }


                    }
                });
        showRemainingCount();
    }

    private void showRemainingCount() {
        txt_view_remaining_images.setText("" + CameraXCapturedImagePreviewDialogFragment.image_taken_number + "/" + no_of_image_to_be_captured);
    }
}
